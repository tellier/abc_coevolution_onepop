\documentclass[12pt,a4paper,onehalfspacing]{article}
%\usepackage{ngerman}
\usepackage{hyperref}
\hypersetup{colorlinks,
	linkcolor={blue!50!black},
	citecolor={black},
	urlcolor={blue!80!black}}
\usepackage{chngcntr}	% package to set counter for tables and figures
	\counterwithin{figure}{section}	% figures are numbered within each section from 1 to n
	\counterwithin{table}{section} % figures are numbered within each section from 1 to n, put * after counterwithin and section number will not appear
\usepackage{booktabs} % for nice tables
	\setlength\heavyrulewidth{1.5pt} %changes thickness of toprule and bottomrule
	\newcommand{\ra}[1]{\renewcommand{\arraystretch}{#1}}
\usepackage[justification=raggedright,singlelinecheck=false,labelfont={bf,sf}]{caption}  % to change appearance of captions 
\usepackage{epsfig}		% to include eps graphics
%\usepackage{titlesec}
%	\titleformat*{\section}{\Large\bfseries\sffamily}
%	\titleformat*{\subsection}{\large\bfseries\sffamily}
%	\titleformat*{\subsubsection}{\bfseries\sffamily}
\usepackage{amsmath}
\usepackage{float}
\usepackage{array}	% 
\usepackage{longtable} %table over several pages
\usepackage{listings}	% inclusion of code
\usepackage{color}	% allows specification of colors
	\definecolor{mygreen}{rgb}{0,0.6,0}
	\definecolor{mygray}{rgb}{0.4,0.4,0.4}
	\definecolor{mygray2}{rgb}{0.7,0.7,0.7}
	\definecolor{mymauve}{rgb}{0.58,0,0.82}
\usepackage{colortbl} % color in table
	\arrayrulecolor{mygray}
\usepackage{courier} % includes font courier
\usepackage{float} % package for stopping figures to float around
\usepackage{amsmath}
\usepackage{forest}
\usepackage{longtable}
\usepackage[customcolors]{hf-tikz}
\usepackage[bottom]{footmisc}
\setlength{\parindent}{0cm}


\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\ttfamily\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{black},       % keyword style
  language=R,                 	% the language of the code
  otherkeywords={*,...},           % if you want to add more keywords to the set
  numbers=none,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,	                   % sets default tabsize to 2 spaces
  title=\lstname,                   % show the filename of files included with \lstinputlisting; also try caption instead of title
  belowskip=-1.5em,
  aboveskip=1em
}
\usepackage{natbib}
\renewcommand*{\familydefault}{\sfdefault}
\usepackage{lscape}

\author{
  Hanna M\"arkle\\
  \small{hanna.maerkle@tum.de}
}
\title{Manual ABC Coevolution pipeline}
\date{\today}


\begin{document}
\sloppy
\maketitle

\section{Introduction}
The presented pipeline can be used to infer parameters of coevolution using host and parasite polymorphism data from putative coevolutionary loci from several host and parasite individuals across several replicates of a repeated experiments. Therefore, an Approximate Bayesian Computation (ABC) method is used. The simulations within the ABC are based on coupling a coevolutionary model with coalescent simulations. The current version of the pipeline can be run by using one of the three gene-for-gene models described in \cite{Tellier2007a}. The sampling process within the ABC is performed using ABCsampler (Version 1.0) from ABCtoolbox \citep{Wegmann2010}. Estimation is performed using ABCestimator (Version 1.0) from ABCtoolbox \citep{Wegmann2010}. Host and parasite sequence data are simulated by combining a C++-program, which simulates allele trajectories under a gene-for-gene model forward in time, with a modified version of msms \citep{Ewing2010,Tellier2014}. The different parts of the pipeline are linked together by shell-scripts and R-scripts.  
A R-script called \textit{Input\_generator.R} is provided in order to generate all input files necessary to set up the pipeline and to create all necessary input files for running the pipeline under the chosen settings.

\section{Getting started}
\subsection{System requirements}
At the current state the pipeline is intended to be run on Linux (it has been tested on Ubuntu 18.04 and Fedora 27). The pipeline can be either run on a single desktop computer or preferentially on a cluster with a (Sun)GridEngine being installed. 
In order to run the pipeline successfully it is necessary that \textit{R} \citep{RCore} (pipeline was tested using R version 3.4.3 and 3.6.2) is installed on the system. Running the pipeline requires the R-packages \textit{plyr} \citep{Wickham2011} and \textit{ggplot2} \citep{Wickham2016} to be installed. Further you have to have writing access to the folders and files used within pipeline. 
Bash version 4.4.12 was used to test the shell-scripts. Make also sure that you have java and the 32bit version of the C++-standard library installed on your computer (required by \path{ABCsampler} and \path{ABCestimator}).


\subsection{Overview pipeline}
Running the pipeline involves four steps:

\begin{enumerate}
\item Calculating the summary statistics for your observed data
\item Using the so called 'input generator' to setup the pipeline structure
\item Running the sampling part of the ABC
\item Running the estimation part of the ABC
\end{enumerate}

\section{Setting-up and launching the pipeline with standard settings}

\begin{enumerate}
\item Within the current directory change working directory to \textit{ABC\_Coevolution}.
\begin{lstlisting}[language=bash]
cd ABC_Coevolution
\end{lstlisting}

This folder has the following structure:
\begin{itemize}
 \item \textbf{R\_scripts\_programs} contains all the necessary scripts, programs and templates to setup and run the pipeline.
\item \textbf{Documentation} contains the documentation 
\item \textbf{Examples} contains all files to run the examples presented in this manual
\end{itemize}

\item Navigate to the folder \textbf{R\_scripts\_programs}.
\begin{lstlisting}[language=bash]
cd R_scripts_programs
\end{lstlisting}
The \textbf{only} file in this folder which you are supposed to change is \textbf{ABC\_GFG\_pseudo\_observed.txt}. This is standard file for storing the summary statistics of your (pseudo-)\-observed data.
\end{enumerate}


\subsection{Calculate summary statistics for your (pseudo)-observed data sets}
\begin{enumerate}
\item Calculate all the summary statistics shown in Tab.~\ref{tab:sumstats} for your (pseudo)observed data. For the formula used to calculate these statistics in the pipeline see section \ref{sec:calcumstats} or the references in Tab.~ \ref{tab:sumstats}. 

\item Insert the calculated summary statistics into the file \path{ABC_GFG_pseudo_observed.txt}. Each row corresponds to a single (pseudo)-observed data set. When saving the file make sure that:
	\begin{itemize}
	\item Columns are separated by \textbf{tabs}.
	\item That you have calculated all summary statistics shown in Tab.~\ref{tab:sumstats}.
	\item That you did not insert any additional columns to the file.
	\end{itemize}
Alternatively, you can use the POD-Generator pipeline to generate a (pseudo)observed data set.

\end{enumerate}

\subsection{Setting up the pipeline}
\begin{enumerate}
\item Set up the standard pipeline by running the R-script \path{Input_generator.R}. For the standard settings of the pipeline see Section \ref{sec:standard}. To launch the script for the pipeline setup type:

\begin{lstlisting}[language=bash]
Rscript --vanilla Input_generator.R
\end{lstlisting}

\item Once the pipeline is set-up launch the sampling part of the pipeline by typing the following two commands into the command line
\begin{lstlisting}[language=bash]
cd ABC_GFG_pipeline_folder/ABC_GFG_sampling
./ABC_GFG_sampling.sh
\end{lstlisting}
This will run 10 simulations with the settings specified for the standard test pipeline.

\item Once the sampling process is completed go to the directory \path{ABC_GFG_pipeline_folder} and start the estimation process.
\begin{lstlisting}[language=bash]
cd ..
./ABC_Collect_Estimate.sh input_collect_estimate.input
\end{lstlisting}
This shell script processes the simulations done in the first part and launches the estimation process a) using host and parasite information together (\path{ABC_GFG_estimation_together}), b) using only host (\path{ABC_GFG_estimation_honly}) information and c) using only parasite information (\path{ABC_GFG_estimation_ponly}).

\end{enumerate}


\section{Standard settings}
\label{sec:standard}

The standard settings are:
\begin{itemize}
\item The pipeline is placed in a folder called \path{ABC_GFG_pipeline_folder} within the directory \path{R_scripts_programs}
\item The standard prefix \textbf{PREFIX} for the whole pipeline is \textbf{ABC\_GFG}. This prefix is appended to all important results and input files generated during the setup of the pipeline and while running the pipeline. 
\item The directory structure which is created when running the input generator is:

\begin{forest}
  for tree={
    font=\ttfamily,
    grow'=0,
    child anchor=west,
    parent anchor=south,
    anchor=west,
    calign=first,
    edge path={
      \noexpand\path [draw, \forestoption{edge}]
      (!u.south west) +(7.5pt,0) |- node[fill,inner sep=1.25pt] {} (.child anchor)\forestoption{edge label};
    },
    before typesetting nodes={
      if n=1
        {insert before={[,phantom]}}
        {}
    },
    fit=band,
    before computing xy={l=15pt},
  }
[\path{ABC_GFG_pipeline_folder}
  [\path{ABC_GFG_estimation_honly}]
  [\path{ABC_GFG_estimation_ponly}]
  [\path{ABC_GFG_estimation_together}]    
  [\path{ABC_GFG_sampling}
    [\path{ABC_GFG_results}]
  ]
]
\end{forest}

By default this folder structure is created within the folder \path{R_scripts_programs}. The folder \path{ABC_GFG_sampling} contains all input and programs required to run the sampling part of the pipeline. The folder \path{ABC_GFG_estimation_together} contains all files to run parameter estimation using host and parasite polymorphism data together. The folder \path{ABC_GFG_estimation_honly} includes all files to run parameter estimation using host polymorphism data only and the folder \path{ABC_GFG_estimation_ponly} includes all files to run parameter estimation based on parasite polymorphism data.

\item The standard items being placed in these folders during pipeline setup are:

\begin{scriptsize}
\begin{forest}
  for tree={
    font=\ttfamily,
    grow'=0,
    child anchor=west,
    parent anchor=south,
    anchor=west,
    calign=first,
    edge path={
      \noexpand\path [draw, \forestoption{edge}]
      (!u.south west) +(7.5pt,0) |- node[fill,inner sep=1.25pt] {} (.child anchor)\forestoption{edge label};
    },
    before typesetting nodes={
      if n=1
        {insert before={[,phantom]}}
        {}
    },
    fit=band,
    before computing xy={l=15pt},
  }
[\textbf{ABC\_GFG\_pipeline\_folder}
  [ABC\_Collect\_Estimate.sh]
  [ABCestimator]
  [\textbf{ABC\_GFG\_estimation\_honly}
  	[ABC\_GFG\_honly\_ABCestimator.input]
  	[ABC\_GFG\_observed\_honly.txt]
  ]	
  [\textbf{ABC\_GFG\_estimation\_ponly}
  	[ABC\_GFG\_ponly\_ABCestimator.input]
  	[ABC\_GFG\_observed\_ponly.txt]  
  ]
  [\textbf{ABC\_GFG\_estimation\_together}
  	[ABC\_GFG\_ABCestimator.input]
  	[ABC\_GFG\_observed.txt]
  ]
  [\textbf{ABC\_GFG\_sampling}
    [ABC\_GFG\_ABCsampler.input]
    [ABC\_GFG.est]
    [ABC\_GFG\_input\_simulationGFG.txt]
    [ABC\_GFG\_observed.txt]
    [\textbf{ABC\_GFG\_results}]    
    [ABC\_GFG\_sampling.sh]
    [ABCsampler]
    [Another\_Cpp]
    [extractSumstatsABCRfuns.R]
    [launch\_simulationGFG.sh]
    [msms3.2rc-b44-pav.jar]
  ]  
  [Inference\_vs\_trueParams\_vc.R]
  [input\_collect\_estimate.input]
  [plotPosteriorsGLM\_modified.R]
  [Process\_sampler\_results.R]
]
\end{forest}
\end{scriptsize}

\item During input generation and setup of the pipeline the input-generator will search for a file called \path{ABC_GFG_pseudo_observed.txt} within the current working directory \path{R_scripts_programs} which contains the (pseudo-)observed data. This file has to contain only but all summary statistics specified in \path{template_sumstatformat.txt}. The columns of the file \path{ABC_GFG_pseudo_observed.txt} are expected to be \textbf{tab}-separated by default. Each line in this file is interpreted as a single (pseudo-)observed data set (see also ABCtoolbox manual \url{https://www.cmpg.iee.unibe.ch/services/software/abctoolbox/index_eng.html}).
\item The standard parameters to be inferred and their prior distributions are:
\begin{table}[H]
\centering
\begin{small}
\begin{tabular}{lll}
\toprule 
Parameter & Prior distribution & Description \\ 
\midrule
N\_host & logunif 1000 20000 & diploid host population size \\ 
N\_parasite & logunif 1000 20000 & diploid parasite population size \\ 
s  & unif 0.1 0.9 & cost of infection \\
\bottomrule
\end{tabular}
\end{small}
\end{table}

\item The standard complex parameters in the pipeline sensu ABCtoolbox are:
\begin{table}[H]
\centering
\begin{small}
\begin{tabular}{lp{5cm}p{4.5cm}}
\toprule
Parameter & Equation & Description \\ 
\midrule
N\_host\_haploid & 2N\_host & haploid host population size \\ 
N\_parasite\_haploid & 2N\_parasite & haploid parasite population size \\ 
gen & 3*(N\_host + N\_parasite + abs(N\_host - N\_parasite))  & number of generations to simulate \\
popmut\_host & N\_host/1000 & neutral population mutation rate host \\
popmut\_para & N\_parasite/1000 & neutral population mutation rate parasite \\
\bottomrule
\end{tabular}
\end{small}
\end{table}


\item The standard values being used to create the allele frequency trajectory under a gene-for-gene model \citep{Tellier2007a} are:

\begin{small}
\begin{longtable}{lp{3.5cm}p{4cm}}
\toprule
Parameter & Value & Description \\ 
\midrule 
\endhead
cH & 0.05 & cost of resistance \\ 
cP & 0.1 & cost of virulence \\ 
c & 1 & cost of non-successful attack \\ 
R0 & 0.2 & initial frequency resistance \\  
a0 & 0.2 & initial frequency inf-allele \\ 
Rtor & 1e-5 & functional mutation rate from RES to res \\  
rtoR & 1e-5 & functional mutation rate from res to RES \\  
Atoa & 1e-5 & functional mutation rate from INF to inf \\ 
atoA & 1e-5 & functional mutation rate from inf to INF \\ 
psi & 1 & auto-infection rate. Will be ignored if model=0 \\
model & 0 & model flavor. 0=monocyclic disease, 1=polycyclic disease. For monocyclic disease check equation 3.1 in \cite{Tellier2007a} \\ 
rDrift & 1 & should drift be included when frequency paths are generated? 0=no, 1= yes \\
prec & 1e-5 & rounding precision \\ 
\midrule
s & based on value drawn by sampler & cost of infection \\
N\_host\_haploid & $=2\text{N\_host}$, therefore, based on the value drawn by the sampler & haploid host population size \\
N\_parasite\_haploid & $=2\text{N\_parasite}$, therefore, based on the value drawn by the sampler & haploid parasite population size \\
gen & 6N\_host  & number of host generations to simulate \\
\bottomrule
\end{longtable} 
\end{small}

\item The standard values for msms are:

\begin{table}[H]
\centering
\begin{small}
\begin{tabular}{llp{4cm}}
\toprule 
Parameter & Value & Description \\ 
\midrule
nsam\_host & 50 & sample size host \\ 
nsam\_para & 50 & sample size parasite  \\ 
N\_host & based on sampler value & diploid population size host \\ 
N\_parasite & based on sampler value & diploid population size parasite \\ 
popmut\_host & $\text{N\_host}/1000$ & neutral population mutation rate host \\ 
popmut\_para & $\text{N\_parasite}/1000$ & neutral population mutation rate parasite \\ 
\bottomrule
\end{tabular} 
\end{small}
\end{table}

\item The standard number of simulations being run per grid point is 250. 200 (corresponds to $r=200$) out of this 250 simulations are randomly chosen to calculate the average summary statistics. If a simulation doesn't succeed to generate host and/or parasite sequences and/or results in zero segregating sites for the host and/or for the parasite it is discarded by default. If the amount of 'successful' simulations drops below 200, this grid point will be ignored during the estimation process.

\item The seed used in the C++-program to run the $i$-th replicate of a grid point is equal to $i$. 
\item The seed being used for running msms for the host for the $i$-th replicate is $i$. The seed being used for running msms for the parasite for the $i$-th replicate is equal to $i+1000$.

\item By default ABCsampler is run for $10$ grid points on a local desktop computer and by using the standard sampler.
\item Subsequent parameter estimation is run:
	\begin{itemize}
	\itemsep 3pt
	\item [-] in folder \path{ABC_GFG_pipeline_folder/ABC_estimation_together} using host and parasite data together\\
	\item [-] in folder \path{ABC_GFG_pipeline_folder/ABC_estimation_ponly} using only parasite summary statistics \\
	\item [-] in folder \path{ABC_GFG_pipeline_folder/ABC_estimation_honly} using only host summary statistics\\ 
	\end{itemize}
\item Parameter stimation using ABCestimator is performed using the standard approach, a dirac peak width of 0.01, a maximum number of 1000 simulations retained and a maximum number of 100,000 simulations read.
\end{itemize}

\begin{landscape}
\begin{table}
\caption{Overview parameter standard settings in the pipeline}
\begin{scriptsize}
\centering
\begin{tabular}{|p{0.2cm}p{2.8cm}p{0.7cm}lp{3cm}lllll|p{4cm}|}
  \hline
 & name & param\-type & value & formula & distribution & first\_param & sec\_param & th\_param & fo\_param & Description\\ 
  \hline
1 & nsam\_host & f & 50 &  &  &  &  &  &  & sample size host\\ 
  2 & nsam\_para & f & 50 &  &  &  &  &  & & sample size parasite \\ 
  3 & nspg & f & 250 &  &  &  &  &  &  & number of simulations per grid point \\ 
  4 & nsimpgu & f & 200 &  &  &  &  &  & & number of simulations per grid point used to calculate the summary statistics \\ 
  5 & nABCrep & f & 10 &  &  &  &  &  &  & total number of grid points to sample in ABC\\ 
  6 & N\_host & p & 5000  &  & logunif & 1000.00 & 20000.00 &  & & diploid host population size \\ 
  7 & N\_parasite & p & 5000 &  & logunif & 1000.00 & 20000.00 &  &  & diploid parasite population size\\ 
  8 & popmut\_host & cp & 5 & N\_host/1000 &  &  &  &  & & population mutation rate host \\ 
  9 & popmut\_para & cp & 5 & N\_parasite/1000 &  &  &  &  & & population mutation rate parasite \\ 
  10 & cH & f & 0.05 &  & unif & 0.01 & 0.40 &  &  & cost of resistance \\ 
  11 & s & p & 0.2 &  & unif & 0.10 & 0.90 &  &  & cost of infection\\ 
  12 & cP & f & 0.10 &  & unif & 0.01 & 0.90 &  &  & cost of virulence\\ 
  13 & cost & f & 1.00 &  &  &  &  &  &  & cost of non-successful attack\\ 
  14 & R0 & f & 0.20 &  &  &  &  &  &  & initial frequency of RES-hosts\\ 
  15 & a0 & f & 0.20 &  &  &  &  &  &  & initial frequency of INF-parasites\\ 
  16 & N\_host\_haploid & cp & 10000  & 2*N\_host &  &  &  &  &  & haploid host population size\\ 
  17 & N\_parasite\_haploid & cp &  10000 & 2*N\_parasite &  &  &  &  & & diploid host population size  \\ 
  18 & gen & cp & 30000 & 3*(N\_host + N\_parasite + abs(N\_host - N\_parasite)) &  &  &  &  &  & number of generations to simulate \\ 
  19 & psi & f & 1.00 &  &  &  &  &  &  & auto-infection rate\\ 
  20 & Rtor & f & 10-5 &  &  &  &  &  &  & mutation rate RES to res\\ 
  21 & rtoR & f & 10-5 &  &  &  &  &  &  & mutation rate res to RES\\ 
  22 & Atoa & f & 10-5 &  &  &  &  &  &  & mutation rate ninf to INF\\ 
  23 & atoA & f & 10-5 &  &  &  &  &  &  & mutation rate INF to ninf\\ 
  24 & prec & f & 10-5 &  &  &  &  &  &  & precision used to simulate coevolutionary trajectory\\ 
  25 & model & f & 0 &  &  &  &  &  & & model to run  (0=monocyclic) \\ 
  26 & rDrift & f & 1 &  &  &  &  &  &  & include drift in generation of allele frequency path \\ 
   \hline  
\end{tabular}
\end{scriptsize}
\end{table}
\end{landscape}


\section{Changing standard settings}
There are numerous ways to change default settings of the pipeline while running the R-script \path{Input_generator.R}. 

\subsection{Option -file: Change parameter settings}

	The option \textbf{-file} can be used to change the parameters to be inferred using the pipeline and also to change the values of fixed parameters. This option has to be specified with two arguments. The first argument is the absolute path to the file which contains the parameter settings to be used, the second argument is the column separator used in this file. 
	The input file has to contain the following nine columns: 
		
		\begin{longtable}{l>{\raggedright\arraybackslash}p{10cm}}
		\toprule 
		column name & description \\ 
		\midrule 
		name & Name of the parameter to be changed. Possible names are: nsam\_host, nsam\_para, nspg, nsimpgu, nABCrep, N\_host, N\_parasite, cH, s, cP, cost, R0, a0 , psi, Rtor, rtoR, Atoa, atoA, prec, model, rDrift \\ 
		paramtype & Type of the parameter specified in column: name. Options are: \textit{p}= parameter to be inferred, \textit{cp}= complex parameter sensu ABCtoolbox, \textit{f}= fixed parameter \\ 
		value & value for parameters with paramtype=\textit{f} \\ 
		formula & formula to be used to calculate the value of a complex parameter\\ 
		distribution & type of prior distribution to be used for a parameter to be inferred (paramtype=\textit{i}). Possible options are: \textit{unif} (uniform distribution), \textit{logunif} (loguniform distribution), \textit{norm} (truncated normal distribution) and \textit{lognorm} (truncated lognormal distribution). See also ABCtoolbox manual for further explanations. \\ 
		first\_param & First argument for the distribution specified in column distribution. If the distribution is unif or logunif the value will interpreted as lower bound of the distribution. If the distribution is norm or lognorm the value will be interpreted as the mean of the distribution. See also ABCtoolbox manual for further explanations.\\ 
		sec\_param & Second argument for the distribution specified in column distribution. If the distribution is \textit{unif} or \textit{logunif} the value be interpreted as the upper bound of the distribution. If the distribution is \textit{norm} or \textit{lognorm} the value be interpreted as the standard deviation of the distribution. See also ABCtoolbox manual. \\ 
		th\_param & Third argument for the distribution specified in column distribution. This field has to be empty or NA if the distribution is \textit{unif} or \textit{logunif}. If the distribution is \textit{norm} or \textit{lognorm} the value be interpreted as the lower bound of the truncated distribution. See also ABCtoolbox manual.\\  
		fo\_param & Forth argument for the distribution specified in column distribution. This field has to be empty or NA if the distribution is \textit{unif} or \textit{logunif}. If the distribution is \textit{norm} or \textit{lognorm} the value will be interpreted as the upper bound of the truncated distribution. See also ABCtoolbox manual.\\ 
		\bottomrule
		\end{longtable} 
		
	These columns can be in arbitrary order. Please note that the parameter type for parameters \textbf{nspg, nsimpgu, nABCresp, prec, rDrift, nsam\_host and nsam\_para} can be only \textit{f} (=fixed). For these parameters you can only specify another fixed value via the input file. If you try to override the parameter type for any of those parameters by specifying paramtype as \textit{cp or p} in the input file, your specification will be automatically ignored and the standard values will be used.
	If you specify a paramter twice in your input file input generation will be aborted.
	
	Values in the columns \textit{name} and \textit{paramtype} have to be provided for all parameters which are specified in the input file.
	Depending on the paramtype of a given parameter the following columns have to be specified in addition:
	
	\begin{itemize}
	\itemsep 3pt
	\item [-] paramtype \textit{p}: distribution, first\_param, sec\_param, (th\_param), (fo\_param)
	\item [-] paramtype \textit{cp}: formula
	\item [-] paramtype \textit{f}: value 
	
	\end{itemize} 
	
	The range, dataype, minimum and maximum values for each parameter can be found in Tab. \ref{tab:ParamMinMax}.
		
	\begin{table}
	\caption{Information concerning the parameters. Datatype indicates if the parameter is treated as integer (1) or double (0). Column r\_by indicates which part of the pipeline mainly use the parameter specified by name (ms= msms, ABCsamp= ABCsampler, C=C++-program), min\_val indicates the minimum value a parameter is allowed to take when being defined by the user, max\_val indicates the maximum value a parameter is allowed to take when being defined by the user.}
	\centering
	\begin{tabular}{rlrlrr}
	  \hline
	 & name & datatype & r\_by & min\_val & max\_val \\ 
	  \hline
	1 & nsam\_host & 1 & ms & 5 & 200 \\ 
	  2 & nsam\_para & 1 & ms & 5 & 200 \\ 
	  3 & nspg & 1 & ws & 1 & 250 \\ 
	  4 & nsimpgu & 1 & ws & 1 & 200 \\ 
	  5 & nABCrep & 1 & ABCsamp & 1 & 100000 \\ 
	  6 & N\_host & 1 & ms & 250 & 100000 \\ 
	  7 & N\_parasite & 1 & ms & 250 & 100000 \\ 
	  8 & popmut\_host & 0 & ms & 1e-2 & 400.00 \\ 
	  9 & popmut\_para & 0 & ms & 1e-2 & 400.00 \\ 
	  10 & cH & 0 & C & 0 & 1 \\ 
	  11 & s & 0 & C & 0 & 1 \\ 
	  12 & cP & 0 & C & 0 & 1 \\ 
	  13 & cost & 0 & C & 0 & 1 \\ 
	  14 & R0 & 0 & C & 0 & 1 \\ 
	  15 & a0 & 0 & C & 0 & 1 \\ 
	  16 & N\_host\_haploid & 1 & C & 500 & 200000 \\ 
	  17 & N\_parasite\_haploid & 1 & C & 500 & 200000 \\ 
	  18 & gen & 1 & C & 10 & 600000 \\ 
	  19 & psi & 0 & C & 0 & 1 \\ 
	  20 & Rtor & 0 & C & 0 & 0.1 \\ 
	  21 & rtoR & 0 & C & 0 & 0.1 \\ 
	  22 & Atoa & 0 & C & 0 & 0.1 \\ 
	  23 & atoA & 0 & C & 0 & 0.1 \\ 
	  24 & prec & 0 & C & 1e-9 & 0.1 \\ 
	  25 & model & 1 & C & 0 & 1 \\ 
	  26 & rDrift & 1 & C & 0 & 1 \\ 
	   \hline
	\end{tabular}
	\label{tab:ParamMinMax}
	\end{table}
		
	If you specify a formula for a complex parameter please check the available options in the ABCtoolbox manual. The input generator will perform checks based on the rules described there. Please note that the input generator does not allow for equations starting with a minus. A valid equation has to only consist of letters, numbers, dots, plus signs, minus signs, multiplication signs, division signs, underscores, opening round brackets and closing round brackets. All whitespaces in the equation will be automatically removed. Although, the synatax of your provided equation is checked carefully please check on your own that the equation cannot run out of the minimum and maximum range of the parameters or results in complex numbers.
	
	For all parameters which are not specified in the input file the standard settings are used. Be careful: If you don't want to infer N\_host, N\_parasite and/or s you have to include this (these) parameters as fixed in the input file as the default setting is to infer this (these) parameters.\\	
	It is possible to use any of the column separators in Tab.~\ref{tab:Separators}.\\
	
	\textbf{Example:} You would like to infer only the cost of infection while fixing the diploid host population size $\text{N\_host}=5000$ and the diploid parasite population size $\text{N\_parasite}=5000$. As by default the population mutation rate of the host (popmut\_host) and the parasite (popmut\_parasite) and the number of generations to simulate (gen) are defined as complex parameters sensu ABCtoolbox you will also have to set the paramtype of these parameters to fixed in the input file and provide values for them. Let's assume we would like to fix them to the following values: popmut\_host=5, popmut\_parasite=5 and gen=30000. Then the resulting input file has to look like the file \path{test_filein.txt} in \path{Examples}. The columns of this file are tab-separated. Let's assume the folder \path{ABC\_Coevolution} is located in \path{home/Documents/}. Then the corresponding command for setting up the pipeline has to look like follows:
	
		\begin{lstlisting}[language=bash]
		Rscript --vanilla Input_generator.R -file ~/Documents/ABC_Coevolution/Examples/test_filein.txt tab
		\end{lstlisting}
	
	

\subsection{Option -prefix: Changing the prefix of the pipeline}

	Using this option will change the prefix of the pipeline. The option has to be followed by one argument namely the the prefix to be used (i.e. \path{NEWPREFIX}). This automatically changes the prefix from the standard prefix \path{ABC_GFG} to \path{NEWPREFIX}. 
	Prefixes have to start always with a letter and can only consist of numbers, letters and underscores.
	Be aware that using the option prefix also implies that the standard name of the file with observed data changes to \path{NEWPREFIX_pseudo_observed.txt}. If your observed data set has a different name you have to also specify the option \textbf{-obsData}.\\

   	\textbf{Example:}\\
   	You would like to use the prefix \path{Test_pipeline} without changing any of the other options. To do so, you have to launch the following command:
   	
   	\begin{lstlisting}[language=bash]
   	Rscript --vanilla Input_generator.R -prefix Test_pipeline
   	\end{lstlisting}
   	
   	Please remember that in this case you have to rename the file with the (pseudo-)observed data from \path{ABC_GFG_pseudo_observed.txt} to \path{Test_pipeline_pseudo_observed.txt}
   	
\subsection{Option -wdout: Changing the directory where the pipeline is placed to}

	To place the pipeline in a folder other than \path{R_scripts_programs} you can launch the input generator together with the option \textbf{-wdout} followed by the absolute path of the directory where you would like to put the pipeline.\\
	
	\textbf{Example:}
	You would like to place the ABC-pipeline into the folder \path{~/Documents/Pipeline_test}
	
	\begin{lstlisting}[language=bash]
	Rscript --vanilla Input_generator.R -wdout ~/Documents/Pipeline_test
	\end{lstlisting}
If the specified working directory does not already exist it will be created on the 'fly'.
	There is a single exception: If you just put a name as i.e. \path{test_wddout} without any absolute path the pipeline will be placed into a directory called \path{test_wdout} within the directory \path{R_scripts_programs}.
	
	\begin{lstlisting}[language=bash]
	Rscript --vanilla Input_generator.R -wdout test_wdout
	\end{lstlisting}

\subsection{Option -obsData: Changing location and name of the file which contains the observed data}

	This option has to be followed by two arguments: The absolute path to the file which contains the observed data and the column separator used for this file. Make sure that the file with the observed data contains only and all the summary statics which are listed in the file \path{template_sumstatformat.txt}.\\
	You can use any column separators described in Tab.~\ref{tab:Separators} for this file.

	\textbf{Example:}
	You have stored your observed data in the file \path{/Examples/test_observed.txt} and columns in the file are separated by a semicolon. In this case you have to launch the pipeline generation with the following command:
		\begin{lstlisting}[language=bash]
		Rscript --vanilla Input_generator.R -obsData ~/ABC_Coevolution/Examples/test_observed.txt semi
		\end{lstlisting}
	(Assuming that the folder \path{ABC_Coevolution} is located in home.)

\subsection{Option -trueParams: Providing a file with the true parameters which were used to generate the pseudo-observed data}

	This option requires two arguments. The first argument is the absolute path to the file which contains the true parameter values of the pseudo-observed data. The second argument is the column separator used for this file. Please make sure that you provide the true values for all parameters which should be inferred (paramtype \textit{p}).
	The column separator has to be any of the separators shown in Tab. \ref{tab:Separators}.
			
	\textbf{Example:}
	You have stored the true parameters of the pseudo-observed data set in the file \path{/Examples/true_params.txt} and columns in the file are separated by comma. You have to launch the following command:			
			\begin{lstlisting}[language=bash]
			Rscript --vanilla Input_generator.R -trueParams ~/ABC_Coevolution/Examples/true_params.txt comma
			\end{lstlisting}
			
\subsection{Option -where: Running ABCsampler on several cores of a Grid\-Engine}

	By using the option \textbf{-where} you can specify whether you would like to launch the sampling process on a single core of a desktop computer or on several cores of a (Sun)GridEngine. This option has to be followed by one mandatory and one optional argument. The first (mandatory) argument must be either \textbf{desktop} in order to run on a desktop or \textbf{SGE} to run on a (Sun)GridEngine. 
	
	If you specfiy \textbf{SGE} as a first argument you have to necessarily provide a second argument. This second argument has to be an integer number (writing something like 1e+2 is not possible) and specifies how many times you would like to launch the ABCsampler in parallel on the (Sun)GridEngine. The total number of grid points to be simulated will be equally distributed among these processes. I.e. you want to run an ABC based on 100,000 simulations and you specify the option \textbf{-where SGE 50}. This will generate the necessary input to run the ABCsampler on 50 cores of the GridEngine and on each of the cores 20,000 simulations will be run.
	
	If use this option in conjuction with the first argument \textbf{SGE} an additional shell script called \path{PREFIX_launch_parallel.sh} will be placed into folder \path{PREFIX_ABC_sampling}. This shell script is necessary to run the parallized sampling process on the (Sun)GridEngine. It ensures that the ABCsampler on each of the cores is initialized with a different seed (which is equal to the \path{SGE_TASK_ID}) and that results will be placed from the scratch into a folder called \path{PREFIX_results_SGE
	_{SGE_TASK_ID}} within the folder \path{PREFIX_results}.

\begin{table}[H]
	\centering
\caption{Column separators which can be used to launch input generation using input from files. The first column contains the possible separators, the second column the respective argument which has to be specified in order to use the separator indicated in the first column.}
			\begin{tabular}{lll}
			\hline 
			column separator & argument name \\ 
			\hline 
			\textbackslash t & tab  \\ 
			; & semi \\
			, & comma \\
			whitespace & white \\
			\hline
			\end{tabular}  \\
\label{tab:Separators}
\end{table}

\section{Summary statistics}
\label{sec:calcumstats}

\begin{table}[H]
	\caption{Summary statistics which have to be calculated from the (pseudo-)observed data, the name which has to be used in the file which contains the (pseudo-)observed data for each summary statistic and/or the respective reference.}
	\begin{tabular}{lll}
		\toprule
		Name & Summary statistic & reference \\
		\midrule 
		S\_host & number of segregating sites host & \\ 
		thetaW\_host &  $\theta_{W}$ host & \cite{Watterson1975}\\ 
		thetapi\_host & $\theta_{\pi}$ host & \cite{Nei1981b}\\ 
		thetaH\_host & $\theta_{H}$ host & \cite{Fay2000}\\ 
		Hprime\_host & Hprime host & \cite{Zeng2006}\\ 
		tajd\_host & Tajima's D host & \cite{Tajima1989} \\ 
		fulif\_host & Fu and Li's F host & \cite{Fu1993} \\
		fulid\_host & Fu and Li's D host & \cite{Fu1993} \\ 
		S\_para & number of segregating sites parasite & \\ 
		thetaW\_para & $\theta_{W}$ parasite & \cite{Watterson1975} \\
		thetapi\_para & $\theta_{\pi}$ parasite & \cite{Nei1981b} \\ 
		thetaH\_para & $\theta_{H}$ parasite & \cite{Fay2000}\\
		Hprime\_para & Hprime parasit.e &  \cite{Zeng2006} \\  
		tajd\_para &  Tajima's D parasite & \cite{Tajima1989}\\  
		fulif\_para &  Fu and Li's F parasite & \cite{Fu1993}\\  
		fulid\_para & Fu and Li's D parasite & \cite{Fu1993}\\ 
		mean\_manhat & Pairwise manhattan distance & Maerkle and Tellier\\ 
		\bottomrule
	\end{tabular}
	\label{tab:sumstats}
\end{table}


\begin{enumerate}
\item Watterson's estimator of the population mutation rate
\[\theta_W=\frac{S}{a_n}\]
\item theta pi
\[ \theta_{\pi} = \frac{2}{n(n-1)} \cdot \sum_{i=1}^{n-1}{i \cdot (n-i) \cdot \xi_i}\]
\item theta\_H
\[\theta_H=\frac{2}{n(n-1)} \cdot \sum_{i=1}^{n-1}{i^2 \cdot \xi_i}\]
\item Hprime
\[\text{Hprime}=\frac{\theta_{\pi}-\theta_{L}}{\sqrt {v_H}}\]
\item Tajima's D
\[\text{Tajima's D}=\frac{\theta_{\pi}-\theta_{W}}{\sqrt{Te1 \cdot S+Te2 \cdot S \cdot (S-1)}}\]
\item Fu and Li's F
\[\text{Fu and Li's F}=\frac{\theta_{\pi}-\xi_1}{\sqrt{u_F \cdot S+v_F \cdot S^2}}\]
\item Fu and Li's D
\[\text{Fu and Li's D}= \frac{S-a_n \cdot \xi_1}{\sqrt{u_D \cdot S+v_D \cdot S^2}}\]
\end{enumerate}
with:
\[a_n = \sum_{i=1}^{n-1}\frac{1}{i}\]
\[b_n =\sum_{i=1}^{n-1}(1/i^2)\]
\[c_n =2 \cdot \frac{n \cdot a_n-2 \cdot (n-1)}{(n-1) \cdot (n-2)}\]
\[bi2 = \frac{2}{n \cdot (n-1)}\]
\[\theta_L=\frac{1}{n-1} \cdot \sum_{i=1}^{n-1}(i \cdot \xi_i)\]
\[Tb1 = \frac{n+1}{3 \cdot (n-1)}\]
\[Tb2 = \frac{2 \cdot (n^2+n+3)}{9 \cdot n \cdot (n-1)}\]
\[Tc1 = Tb1-\frac{1}{a_n}\]
\[Tc2 = Tb2- \frac{n+2}{(a_n \cdot n)}+ \frac{b_n}{a_n^2}\]
\[Te1=\frac{Tc1}{a_n}\]
\[Te2=\frac{Tc2}{a_n^2+b_n}\]
\[v_D=1+\frac{a_n^2}{b_n+a_n^2}\cdot(c_n-\frac{n+1}{n-1})\]
\[u_D=a_n-1-v_D\]
\[v_F=\frac{c_n + \frac{2 \cdot (n^2+n+3)}{9 \cdot n \cdot (n-1)}-\frac{2}{n-1}}{a_n^2 +b_n}\]
\[u_F=\frac{1+ \frac{n+1}{3 \cdot (n-1)} - 4 \cdot  \frac{n+1}{(n-1)^2} \cdot (a_{n+1} -\frac{2 \cdot n}{n+1})}{a_n} - v_F\]
\[\theta_2= \frac{S \cdot (S-1)}{a_n^2 + b_n}\]
\[v_H=\frac{n-2}{6 \cdot (n-1)} \cdot \theta_W+\frac{18 n^2 \cdot (3n+2) \cdot b_{n+1}-(88n^3+9 n^2-13n+6)}{9n \cdot (n-1)^2} \cdot \theta_2\]


\bibliographystyle{natbib}%%%%Bibliography style file
\bibliography{Literature}%%%bibliography file(.bib)

\end{document}
