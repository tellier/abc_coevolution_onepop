README
Hanna Maerkle

This small example is intended to infer the cost of infection from a r=30 times repeated coevolutionary experiment assuming a polycyclic disease (Model B in Tellier and Brown 2007, main model in Maerkle and Tellier 2020).
Settings are:

nsimpgu 30
nspg 50
N_host 5000
N_parasite	5000
N_host_haploid	10000
N_parasite_haploid	10000
popmut_host	5
popmut_para	5
gen	30000
cH	0.05
s	unif(0.1,0.9)
cost	1
cP	0.1
a0	0.2
R0	0.2
psi	1
Rtor	1e-5		
rtoR	1e-5
Atoa	1e-5
atoA	1e-5
model 1
rDrift	1
nsam_host	50
nsam_para	50
nABCrep	50

The pseudo-oberserved data set has been simulated for a true cost of infection being equal to s=0.25



