#!/bin/bash

# Read input for shell script from file
readarray -t input < input_collect_estimate.input
prefix=${input[0]}
nABCsamp=${input[1]}
nsimpgu=${input[2]}
where=${input[3]}
trueParName=${input[4]}

folder=`pwd`
sdir=${folder}/${prefix}_sampling
resdir=${sdir}/${prefix}_results
estname=${folder}/${prefix}_estimation
estdir=${estname}_together
h_estdir=${estname}_honly
p_estdir=${estname}_ponly

res1=RESULTS.txt

resoutName=${prefix}_results_together.txt

simNamepart=${prefix}_samp_simul
simExt=.sim

plotName=${prefix}
plotExt=.pdf

dana=`date +%d_%m_%Y`

# Calculate the number of columns which contain information about the sampled parameter values
# The result of the calculation will miss one column 
lnsc=`grep -v -E '^$|\[|^//' ${sdir}/${prefix}.est | wc -l`
lnsc=$((lnsc+1))

if [[ $where = "SGE" ]]
then
    cd ${resdir}
    cp `find . -type f -name "${prefix}_sampresult_SGE_*.txt"` ./


    for i in ${prefix}_sampresult_SGE_*.txt
     do awk '{if(NR==1){print "ID\t"$0}else{print FILENAME"_"NR-2"\t" $0}}' $i > `basename $i .txt`.app; 
    done

    # Check if file exists
    [ -e $res1 ] && rm $res1
    [ -e $resoutName ] && rm $resoutName


    # Merge all result files into a single file
    for file in ${prefix}_sampresult_SGE_*.app; 
	do cat $file >> ${res1}; 
    done

    # Get the header of the results file
    header=$(head -n1 ${res1})

    # Check how many time the header of the first file is found in the results file
    sihead=$(grep -x "^$header$" ${res1} | wc -l)

    # Count the number of result files from the different cores
    nfile=$(ls ${prefix}_sampresult_SGE_*.app | wc -l)

    # Draw an error if there are not as many identical headers in the merged file 
    # as there are results files, otherwise remove all the headers in the middle of the file
    # and only keep the top header
    if [[ $nfile != $sihead ]]
    then
	rm ${res1}
	exit 3
    else
	head -n1 ${res1} > ${resoutName}
	grep -vx "^$header$" ${res1} >> ${resoutName}
    fi

    cd ${folder}
else
    cp ${sdir}/${prefix}_sampling1.txt ${resdir}/${resoutName}
fi


# Process sampler results and observed data in such a way that they can used to 
# to estimate the parameters using host and parasite together, using host only
# using parasite only
Rscript --vanilla Process_sampler_results.R ${sdir} ${resdir} ${estdir} ${h_estdir} ${p_estdir} ${resoutName} ${nsimpgu} ${nABCsamp} ${prefix} ${lnsc}

cp ABCestimator ${estname}_together/
cp ABCestimator ${estname}_honly/
cp ABCestimator ${estname}_ponly/

# Inference host and parasite data together
cd ${estdir}
./ABCestimator ${prefix}_ABCestimator.input

Rscript --vanilla ${folder}/plotPosteriorsGLM_modified.R ${prefix}_ABCestimator.input
if [[ ${trueParName} != "NA" ]]
then
	Rscript --vanilla ${folder}/Inference_vs_trueParams_vc.R `pwd` ${plotName} ${plotExt} ${trueParName} ${prefix} black
fi

cd ${h_estdir}
./ABCestimator ${prefix}_honly_ABCestimator.input
Rscript --vanilla ${folder}/plotPosteriorsGLM_modified.R ${prefix}_honly_ABCestimator.input
if [[ ${trueParName} != "NA" ]]
then
	Rscript --vanilla ${folder}/Inference_vs_trueParams_vc.R `pwd` ${plotName}_honly ${plotExt} ${trueParName} ${prefix}_honly darkturquoise
fi

cd ${p_estdir}
./ABCestimator ${prefix}_ponly_ABCestimator.input
Rscript --vanilla ${folder}/plotPosteriorsGLM_modified.R ${prefix}_ponly_ABCestimator.input
if [[ ${trueParName} != "NA" ]]
then
	Rscript --vanilla ${folder}/Inference_vs_trueParams_vc.R `pwd` ${plotName}_ponly ${plotExt} ${trueParName} ${prefix}_ponly darkorange
fi

