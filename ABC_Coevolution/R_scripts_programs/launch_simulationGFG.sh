#!/bin/bash
DirOut=simuls


mkdir -p $DirOut
# read input variables (estimation parameters from input file)
file=$1
read -a param < $file


# extract parameters for simulations of each grid point
nsam_host=${param[0]}
nsam_para=${param[1]}
nspg=${param[2]}
nsimpgu=${param[3]}

# extract parameters needed for msms
N_host=${param[4]}
N_parasite=${param[5]}
popmut_host=${param[6]}
popmut_para=${param[7]}

# change parameter file for coevolution simulation 
cut -f 9-26 ${file}  > input_param_fpath.txt


for ((i=1;i<=${nspg};i++))
do

	perl -i -slane '{@F[14]=$i; print join("\t",@F)}' -- -i=$i input_param_fpath.txt # Write this value to the 15th column of the input file for the coevolution simulation


	#simulate coevolution
	./Another_Cpp input_param_fpath.txt

	cp GFGH.out ${DirOut}/GFGH_${i}.out
	cp GFGP.out ${DirOut}/GFGP_${i}.out
	#delete lines from trace to stabilize msms
	tac GFGH.out | awk 'BEGIN{l=0}{if($2 == 0){l++;}else{l=0;};if(l == 0 || l > 2){print }}'| tac > ${DirOut}/GFGH_${i}new.out
	tac GFGP.out | awk 'BEGIN{l=0}{if($2 == 0){l++;}else{l=0;};if(l == 0 || l > 2){print }}'| tac > ${DirOut}/GFGP_${i}new.out

	rm GFGH.out
	rm GFGP.out

	#simulate host sequence
	java -jar ./msms3.2rc-b44-pav.jar -N $N_host -ms $nsam_host 1 -t ${popmut_host} -Strace ${DirOut}/GFGH_${i}new.out -seed $i > $DirOut/seq_H_${i}.out
	ipara=$(($i+1000))
	#simulate parasite sequence
	java -jar ./msms3.2rc-b44-pav.jar -N $N_parasite -ms $nsam_para 1 -t ${popmut_para} -Strace ${DirOut}/GFGP_${i}new.out -seed ${ipara} > $DirOut/seq_P_${i}.out
done

Rscript --vanilla extractSumstatsABCRfuns.R `pwd`/${DirOut} $nspg $nsimpgu
paste <(echo "INPUT_SHELL") <(cat $file) >> control_simulated_params.txt
paste <(echo "INPUT_C++") <(cat logfile.out) >> control_simulated_params.txt
paste <(echo "INPUT_msms_host") <(head -n 1 simuls/seq_H_${nspg}.out) >> control_simulated_params.txt
paste <(echo "INPUT_msms_para") <(head -n 1 simuls/seq_P_${nspg}.out) >> control_simulated_params.txt


exit 1
