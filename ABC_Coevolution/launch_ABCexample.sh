#!/bin/bash

# Create a new folder within the current working directory
mkdir ExampleFolder
# Get the current working directory
curdir=`pwd`
echo ${curdir}
# Get the path of the directory into which the ABC coevolution pipeline should be placed
expdir=${curdir}/ExampleFolder

# Change working directory to R_scripts_programs
cd R_scripts_programs
echo "---Running input generation ---"
# Setup the pipeline using the specified options
Rscript --vanilla Input_generator.R -prefix Example_ABC_GFG -obsData ../Examples/Files_example_pipeline/ExamplePOD_seedpod_1_pseudo_observed.txt tab -file ../Examples/Files_example_pipeline/Example_parameter_input.txt tab -wdout ${expdir} -trueParams ../Examples/Files_example_pipeline/ExamplePOD_seedpod_1_true_params.txt tab
# Change to the directory where the sampling part of the ABC pipeline has been placed to
cd ${expdir}/Example_ABC_GFG_pipeline_folder/Example_ABC_GFG_sampling
echo "---Running input generation completed"
# Format the results from the sampling process, to allow for running the estimation process using host and parasite information, host information and parasite information and run the estimation
echo "---Running sampling---"
./Example_ABC_GFG_sampling.sh &> log.out
cd ..
echo "---Sampling done---"
pwd
echo "---Running estimation---"
./ABC_Collect_Estimate.sh input_collect_estimate.input
echo "---Done---"
