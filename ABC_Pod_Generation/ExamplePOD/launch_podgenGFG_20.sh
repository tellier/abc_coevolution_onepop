#!/bin/bash

# Shell script to generate pods for testing the ABC with the coeovolutionary model
# Changed 15.11.2016, 16.11.2016, 01.02.2017

if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
	exit
fi

prefix=$1
file=${prefix}_input_pods.txt

if [[ ! -f $file ]]
	then
	echo "$file does not exits. Will abort pod generation"
	exit
fi


while read flines;
do
	read -a param <<< "$flines"		
	# extract parameters for simulations of each grid point
	nsam_host=${param[0]}
	nsam_para=${param[1]}
	nspg=${param[2]}
	nsimpgu=${param[3]}

	# extract parameters needed for msms
	N_host=${param[4]}
	N_parasite=${param[5]}
	popmut_host=${param[6]}
	popmut_para=${param[7]}
	DirOut=${param[26]}
	echo $DirOut

	# extract parameters for simulation of the coevolutionary trajectory 
	echo $flines | cut -d ' ' -f 9-26  > input_simu_pods.txt
	seedtouse="$(cut -d' ' -f28 <<<"$flines")"
	
	mkdir -p $DirOut # Create output directory if it does not already exist
	cd $DirOut
	ls | xargs -I {} mv {} OLD_{}
	cd ..
	
	touch $DirOut/logs_together.out
	touch ${DirOut}/param_used.txt
     
	for((i=1;i<=$nspg;i++)) 
	do
		echo "seed from  file ${seedtouse}"	
		j=$(($i + ${seedtouse}))
		echo "seed is $j" 
		# Set seed for i-th simulation and write to the temporary input file
		perl -i -slane '{@F[14]=$j; print join("\t",@F)}' -- -j=$j input_simu_pods.txt # Write this value to the second column of the input file for the coevolution simulationecho `cut -f 1-11 
		
		#simulate coevolution
		./Another_Cpp input_simu_pods.txt


		#delete lines from trace to stabilize msms
		tac GFGH.out | awk 'BEGIN{l=0}{if($2 == 0){l++;}else{l=0;};if(l == 0 || l > 2){print }}'| tac > $DirOut/GFGH_${i}new.out
		tac GFGP.out | awk 'BEGIN{l=0}{if($2 == 0){l++;}else{l=0;};if(l == 0 || l > 2){print }}'| tac > $DirOut/GFGP_${i}new.out

		# remove unstabilized frequency paths
		 mv GFGH.out $DirOut/GFGH_${i}.out
		 mv GFGP.out $DirOut/GFGP_${i}.out
		 cat logfile.out >> $DirOut/logs_together.out

		#simulate host sequence
		echo "here"
		java -jar ./msms3.2rc-b44-pav.jar -ms ${nsam_host} 1 -t ${popmut_host} -N ${N_host} -Strace $DirOut/GFGH_${i}new.out -seed $j -L > $DirOut/seq_H_${i}.out
		jpara=$(($j+1000))
		#simulate parasite sequence
		java -jar ./msms3.2rc-b44-pav.jar -ms ${nsam_para} 1 -t ${popmut_para} -N ${N_parasite} -Strace $DirOut/GFGP_${i}new.out -seed ${jpara} -L > $DirOut/seq_P_${i}.out
		
		cat input_simu_pods.txt >> ${DirOut}/param_used.txt 
		echo ${i} runs completed
	done

	Rscript --vanilla extractSumstatsABCRfuns_pods.R `pwd`/${DirOut} $nspg $nsimpgu
done < <(tail -n +2 $file)
rm logfile.out 
rm input_simu_pods.txt 

exit 1
