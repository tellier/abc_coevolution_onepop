# Hanna Maerkle
# hanna.maerkle@tum.de
# Script to prepare input for pod generation
# CAUTION:
# This version of the input generator always fixes the population mutation rate of both species being equal to 
# diploid population size/1000, see line 252/253
# and also fixes the number of generations to simulate to max(N_host,N_parasite)*6 generations
# updated 01/27/2020

# Hanna Maerkle
# hanna.maerkle@tum.de

require("plyr",lib.loc="/usr/lib64/R/library")

stop1 <- function(x){stop(x,call.=F)}

# Function to check if the wdout switch has been specified correctly if used
# If not used in input then set working directory to current working directory
check_wdout_sw <- function(wd_sub){
  wd_switchnrs <- which(sapply(wd_sub,function(x){substr(x,1,1)})=="-")
  if(any(wd_sub[wd_switchnrs]!="-wdout")){stop1("Something wired happened. This function should not have been entered")}
  # Check if the right number of arguments has been provided
  if(length(wd_switchnrs)==0){
    wdout <- getwd()
  }else{
    # If the switch wdout has been specified once
    if(length(wd_switchnrs)==1){
      # Check if it comes with two arguments (-wdout + name of wdout)
      if(length(wd_sub)!=2){
        stop("Wrong number of arguments specified with option -wdout")
      }else{
        # Otherwise extract name of working directory
        wdout <- wd_sub[2]
        tryCatch(
          {
            # Check if wdout already exists, if not create it
            if(!dir.exists(wdout)){dir.create(wdout)} 
          },error=function(e){message(paste("Could not create directory",wdout))},
          warning = function(w) {message(w)},
          finally = message(paste("Directory",wdout,"exists"))
        )
      }
    }else{
      # If option has been used several times stop pod generation
      if(length(wd_switchnrs) > 1){
        stop("Option -wdout was specified more than two times")
      }  
    }
  }
  return(wdout)
}  

# Function to check if the seed for the PODs has been specified and if yes if it has been done properly
check_seed_sw <- function(seed_sub){
  # Check all the strings which are suppossedly related to the seed for the POD
  seed_switchnrs <- which(sapply(seed_sub,function(x){substr(x,1,1)})=="-")
  if(any(seed_sub[seed_switchnrs]!="-seedpod")){stop1("Something wired happened")}
  # Check if the right number of arguments has been provided
  # if the seedpod option has not been used, set the seed for the POD(s) to 1
  if(length(seed_switchnrs)==0){
   seedpod <- 1
  }else{
    # Check how often the seedpod option argument has been specified in the command line
    if(length(seed_switchnrs)==1){
      if(length(seed_sub)!=2){
        # If it has been specified once check if it has been specified correctly
        stop("Wrong number of arguments specified with option -seedpod")
      }else{
        # If it has been specified correctly set the seed for the POD(s) to the specified command line argument
        seedpod <- as.numeric(as.character(seed_sub[2]))
      }
    }else{
      # If the option has been specified several times stop POD generation
      if(length(seed_switchnrs) > 1){
        stop("Option -seedpod was specified more than once")
      }  
    }
  }
  return(seedpod)
}  


# Check if the prefix option has been used and if yes if it has been used correctly
check_prefix_sw <- function(p_sub){
  p_switchnrs <- which(sapply(p_sub,function(x){substr(x,1,7)})=="-prefix")
  # Stop POD generation if the prefix has been specified several times
  if(length(p_switchnrs) > 1){stop1("-prefix has been specified at least twice")}
  # If it has not been specified use prefix ABC_GFG
  if(length(p_switchnrs)==0){prefix <- "ABC_GFG"}else{
    # Check if it has been specified once check if it comes with the right number of arguments
    if(length(p_sub)> 2){stop1("-prefix is followed by too many arguments")}else{
      # If it comes with the right number of arguments, extract the prefix
      prefix <- p_sub[2]
      # Check if the prefix meets the prefix specification, if not stop POD generation
      if(!grepl('^[a-zA-Z][a-zA-Z0-9_]+$',prefix)){stop1("User defined prefix has to start with letters and can only contain numbers, letters and underscores")}
    }
  }
  return(prefix)
}

# Check which parameters have been specified for the POD generation and which should be inferred later
check_i_sw <-function(i_sub,optasetspods){
  # Get all entries in the i_sub vector which start with -
  i_switchnrs <- which(sapply(i_sub,function(x){substr(x,1,1)})=="-")
  if(any(i_sub[i_switchnrs]!="-i")){stop1("Oops, something went wrong")}
  # Check if parameters for POD generation have been specified
  if(length(i_switchnrs)==0){
    stop1("No parameters for pod generation have been specified")
  }else{
    # Check if the correct number of arguments has been provided for all -i parameters
    if(length(i_switchnrs)==1){if(length(i_sub)!=5){stop1("Incorrect specification for at least one of the parameters used with option -i")}}else{
      if((length(i_sub)!=(length(i_switchnrs)*5))|(any(( i_switchnrs[-1] - i_switchnrs[-length(i_switchnrs)])!=5)))
      {stop1("Incorrect specification for at least one of the parameters used with option -i")}
    }
  }
  
  # Check if the arguments 2-4 after -i only contain numbers and dots
  if(any(!grepl("^[0-9]+[.]?[0-9]*",i_sub[-c(i_switchnrs,i_switchnrs+1)]))){stop1("Range has to be specified with positive integers")}
  
  # Put all the parameters for pod generation in a data.frame
  paramset <- as.data.frame(matrix(i_sub[-i_switchnrs],byrow=T,ncol=4),stringsAsFactors =F)
  names(paramset) <- c("param","min","max","seq_len")
  # Check if each of the parameters specified with -i option has been only specified once
  if(any(table(paramset$param) > 1)){stop1("At least one parameter has been indicated twice by using -i")}
  # Convert last three columns to numeric
  paramset[,2:4] <- apply(paramset[,2:4], 2, function(x) as.numeric(as.character(x)))
  
  # Extract the allowed range of parameter values for the parameters specified with -i option
  imo <- pmatch(paramset$param,optasetspods[["reqs"]]$name)
  paramset$min[which(optasetspods[["reqs"]]$datatype[imo]==1)] <-round(paramset$min[which(optasetspods[["reqs"]]$datatype[imo]==1)],digits=0)
  paramset$max[which(optasetspods[["reqs"]]$datatype[imo]==1)] <-round(paramset$max[which(optasetspods[["reqs"]]$datatype[imo]==1)],digits=0)
  
  
  # Check if all these parameters can be inferred (if not pod generation does not make sense at all)
  if(any(paramset$param%in%optasetspods$fixedParams)){stop1("At least on parameter you wanted to change during pod generation is in list of fixed parameters")}
  # Check if the provided parameters are in list of available parameters
  if(any(!paramset$param%in%optasetspods$availParams)){stop1(paste("Don't know parameter(s):",paramset$param[which(!paramset$param%in%optasetspods$availParams)]))}
  
  # Check if N_host_haploid has been provided. For pod generation N_host_haploid is not allowed to be provided 
  if("N_host_haploid"%in%paramset$param){stop1("You are not allowed to provide parameter N_host_haploid during pod generation. This parameter will be automatically calculated as 2*N_host")}
  # Check if N_host is provided. If yes indicate that it has been provided  
  if("N_host"%in%paramset$param){i_host <- TRUE}else{i_host <- FALSE}
  
  # Check if N_host_haploid has been provided. For pod generation N_host_haploid is not allowed to be provided 
  if("N_parasite_haploid"%in%paramset$param){stop1("You are not allowed to provide parameter N_parasite_haploid during pod generation. This parameter will be automatically calculated as 2*N_parasite")}
  # Check if N_host is provided. If yes indicate that it has been provided  
  if("N_parasite"%in%paramset$param){i_para <- TRUE}else{i_para <- FALSE}
  
  # Check that the parameters are within the range of possible values
  if(any(paramset$min > paramset$max)){
    stop1(paste("Parameter(s)",paste(paramset$param[which(paramset$min > paramset$max)],collapse=", "),": specified minimum is larger than specified maximum"))
  }
  
  if(any(paramset$min < optasetspods[["reqs"]][pmatch(paramset$param,optasetspods[["reqs"]]$name),"min_val"])){
    stop1(paste("Parameter(s)",paste(paramset$param[which(paramset$min < optasetspods[["reqs"]][pmatch(paramset$param,optasetspods[["reqs"]]$name),"min_val"])],collapse=", "),": specified minimum is out of range"))
  }
  
  if(any(paramset$max > optasetspods[["reqs"]][pmatch(paramset$param,optasetspods[["reqs"]]$name),"max_val"])){
    stop1(paste("Parameter(s)",paste(paramset$param[which(paramset$value > optasetspods[["reqs"]][pmatch(paramset$param,optasetspods[["reqs"]]$name),"max_val"])],collapse=", "),": specified maximum is out of range"))
  }
  return(list(paramset=paramset,i_host=i_host,i_para=i_para))
}

# Check if the sc option has been used, and if yes whether it has been used in the correct way
check_sc_sw <- function(sc_sub,paramset,optasetspods){
  # Check if any of the standard value has to be changed
  sc_switchnrs <- which(sapply(sc_sub,function(x){substr(x,1,1)})=="-")
  if(any(sc_sub[sc_switchnrs]!="-sc")){stop("Ooops, something went wrong")}
  # If the option has been used at least once check if the right number of arguments is provided
  if(length(sc_switchnrs) > 0){
    if(length(sc_switchnrs)==1){if(length(sc_sub)!=3){stop1("Invalid number of arguments for at least one of the parameters specified with option -sc")}}else{
      if((length(sc_sub)!=(length(sc_switchnrs)*3))|(any(( sc_switchnrs[-1]-sc_switchnrs[-length(sc_switchnrs)])!=3)))
      {stop("Invalid number of arguments for at least one of the parameters specified with option -sc")}
    }
    # Check if the arguments 2 after -sc only contain numbers and dots
    if(any(!grepl("^[0-9]+[.]?[0-9]*",sc_sub[-c(sc_switchnrs,sc_switchnrs+1)]))){stop1("Range has to be specified with positive integers")}
    fset <- as.data.frame(matrix(sc_sub[-sc_switchnrs],byrow=T,ncol=2),stringsAsFactors =F)
    names(fset) <- c("param","value")
    fset[,2] <-as.numeric(as.character(fset[,2]))
    scmo <- pmatch(fset$param,optasetspods[["reqs"]]$name)
    fset$value[which(optasetspods[["reqs"]]$datatype[scmo]==1)] <-round(fset$value[which(optasetspods[["reqs"]]$datatype[scmo]==1)],digits=0)
    
    # Check if the same parameter has specified for -p and -sc
    if(any(table(c(fset$param,paramset$param))>1)){stop1("At least one parameter has been specified using option -i and option -sc")}
    if(any(table(fset$param) > 1)){stop1("At least one parameter has been indicated twice by using -sc")}
    fset$value <- as.numeric(as.character(fset$value))
    
    if(any(!fset$param%in%optasetspods$availParams)){stop1(paste("Do know parameter(s):",fset$param[which(!fset$param%in%optasetspods$availParams)]))}
    # Change if the provided fixed parameter values are in the range of acceptable values
    if(any(fset$value < optasetspods[["reqs"]][pmatch(fset$param,optasetspods[["reqs"]]$name),"min_val"])){
      stop1(paste("Parameter(s)",paste(fset$param[which(fset$value < optasetspods[["reqs"]][pmatch(fset$param,optasetspods[["reqs"]]$name),"min_val"])],collapse=", "),"is/are too small"))
    }
    if(any(fset$value > optasetspods[["reqs"]][pmatch(fset$param,optasetspods[["reqs"]]$name),"max_val"])){
      stop1(paste("Parameter(s)",paste(fset$param[which(fset$value > optasetspods[["reqs"]][pmatch(fset$param,optasetspods[["reqs"]]$name),"max_val"])],collapse=", "),"is/are too small"))}
    
    
    # Check if N_host_haploid has been provided. For pod generation N_host_haploid is not allowed to be provided 
    if("N_host_haploid"%in%fset$param){stop1("You are not allowed to provide parameter N_host_haploid during pod generation. This parameter will be automatically calculated as 2*N_host")}
    # Check if N_host is provided. If yes indicate that it has been provided  
    if("N_host"%in%fset$param){sc_host <- TRUE}else{sc_host <- FALSE}
    
    # Check if N_host_haploid has been provided. For pod generation N_host_haploid is not allowed to be provided 
    if("N_parasite_haploid"%in%fset$param){stop1("You are not allowed to provide parameter N_parasite_haploid during pod generation. This parameter will be automatically calculated as 2*N_parasite")}
    # Check if N_host is provided. If yes indicate that it has been provided  
    if("N_parasite"%in%fset$param){sc_para <- TRUE}else{sc_para <- FALSE}
    
    # Replace the values in optasetpods with the checked used provided parameter values
    optasetspods[["paramdat"]][pmatch(fset$param,optasetspods[["paramdat"]]$name),"value"] <- fset$value
    # Make sure that nsimpgu < nspg
    if(optasetspods[["paramdat"]][which(optasetspods[["paramdat"]]$name=="nsimpgu"),"value"] > optasetspods[["paramdat"]][which(optasetspods[["paramdat"]]$name=="nspg"),"value"]){stop1("nsimpgu > nspg")}
    # If N_host or N_parasite has been specified with option -sc recalculate the corresponding haploid population size
    if(sc_para){optasetspods[["paramdat"]][which(optasetspods[["paramdat"]]$name=="N_parasite_haploid"),"value"] <- fset$value[which(fset$param=="N_parasite")]*2}
    if(sc_host){optasetspods[["paramdat"]][which(optasetspods[["paramdat"]]$name=="N_host_haploid"),"value"] <- fset$value[which(fset$param=="N_host")]*2}
  }
  return(optasetspods)
}

# Create all combinations of the parameters which have been specified with -i option and create a unique key for the POD
create_true_params <- function(paramset,prefix){
  # Calculate the amount of all unique combinations
  print(prod(paramset$seq_len) ) 
  # If the number of combination is larger than 160 stop POD generation
  if(prod(paramset$seq_len) > 160){stop1("Too many parameter combinations have been specified for pod generation")}
  # Print the parameter specifications
  print(paramset)
  # Create all unique combinations
  true_params <- expand.grid(lapply(apply(paramset[,-1],1,function(x){a <- as.numeric(x);return(list(seq(a[1],a[2],len=a[3])))}),"[[",1))
  print(paramset$param)
  print(names(true_params))
  names(true_params) <- paramset$param
  # Add a key to each row of true_params
  # Key will be as follows: Pods_PREFIX_VAR1_VALVAR1_VAR2_VALVAR2_...
  key <- paste("Pod",prefix,names(true_params)[1],true_params[,1],sep="_")
  if(ncol(true_params) > 1){
    for(cn in 2:ncol(true_params)){
    key <- paste(key,names(true_params)[cn],true_params[,cn],sep="_")
  }
  }  
  true_params$key <- key
  return(true_params)
}

source("functions_input_generator.R")

######################
### MAIN #############
print("This is ABC-PODgenerator\n")
print("!!!!CAUTION: This version of the input generator always fixes the population mutation rates to N_host/1000,\n
      N_parasite/1000 and\nsimulates the trajectory for gen=max(N_host,N_parasite)*6 generations. Settings which you will enter for these parameters\n
      in the command line will been overwritten. To avoid this behavior go outcomment lines 324-326 of this R-script!!!")
# Create a list with the standard settings using function availOpts in functions_input_generator.R
optasetspods <- c(availOpts()[c("availParams","reqs","fixedParams","paramdat")],list(pod_programs=c("Another_Cpp","msms3.2rc-b44-pav.jar",
                                                                                                       "extractSumstatsABCRfuns_pods.R","launch_podgenGFG_20.sh")),list(seedpod=1))
# Read command line arguments
comArgs <- as.character(commandArgs(trailingOnly=T))
# Paste command line arguments into a single string
paramstring <- paste(comArgs,collapse=" ")

# Find all substrings which start with - and extract these substrings into a vector
subparamstring <- substring(paramstring,gregexpr("-",paramstring)[[1]],c(gregexpr("-",paramstring)[[1]][-1]-1,nchar(paramstring)))

# Find all substrings which start with an -i, split these by empty space and coerce to a single vector
i_part <- grep("^-i ",subparamstring)
sc_part <- grep("^-sc ",subparamstring)
p_part <- grep("^-prefix ",subparamstring)
wd_part <- grep("^-wdout ",subparamstring)
seed_part <- grep("^-seedpod ",subparamstring)
if(sum(length(i_part),length(sc_part),length(p_part),length(wd_part),length(seed_part)) < length(subparamstring)){stop1("Incorrect specification of command line arguments")}

i_sub <- unlist(strsplit(subparamstring[i_part]," "))
# Find all substrings which start with -sc, split these by empty space and coerce to a single vector
sc_sub <-  unlist(strsplit(subparamstring[sc_part]," "))
# Find all substrings which start with -prefix, split these by empty space and coerce to a single vector
p_sub <- unlist(strsplit(subparamstring[p_part]," "))
# Find all substrings which start with wdout
wd_sub <- unlist(strsplit(subparamstring[wd_part]," "))
# Find all substrings which start with seedpod
seed_sub <- unlist(strsplit(subparamstring[seed_part]," "))



# Check if wdout has been specified. If yes create the directory if it doesn't exist yet
wdout <- check_wdout_sw(wd_sub)
# Check if prefix has been specified. If yes use the user specified prefix, if it is specified correctly. If no return standard prefix ABC_GFG
prefix <- check_prefix_sw(p_sub)
# Check if seed has been specified
seedpod <- check_seed_sw(seed_sub)

# Check for which values pods are suppossed to be created. Returns a list with three entries. A matrix containig the instructions to create the
# changing values for the pods,  a logical named i_host which indicates if pods are generated for changing population sizes and another logical
# called i_para which inidicates if pods are generated for changing parasite population sizes
res_i_sw <- check_i_sw(i_sub,optasetspods)
list2env(res_i_sw ,.GlobalEnv)

# Check if one of the fixed values for pod generation has to be different from standard value. If yes replace by this value in 
# data.frame paramdat of list optasetspods
optasetspods <- check_sc_sw(sc_sub,paramset,optasetspods)

# Extend the prefix with the number which have been given to the POD
prefix <- paste(prefix,"seedpod",seedpod,sep="_")
# Create all possible combinations of the values to be considered in the pod generation
true_params <- create_true_params(paramset=paramset,prefix=prefix) 

paramdat <- optasetspods[["paramdat"]]
paramdatrr <- paramdat[-which(paramdat$name%in%c("nABCrep",paramset$param,"N_host_haploid"[i_host],"N_parasite_haploid"[i_para])),]

#########################################################################################################################
# Create simulation schedule
# First read the template file
tempfile_in <- "template_input_simulationProg.txt"

if(!file.exists(tempfile_in)){"Template file for simulation of model is missing"}
snames <- temp <- as.character(read.table(file=tempfile_in,header=F,stringsAsFactors=F))
pnmt <- pmatch(paramdatrr$name,temp)
# DO NOT CHANGE
podnmt <- pmatch(c(paramset$param,"N_host_haploid"[i_host],"N_parasite_haploid"[i_para]),temp)
temp[pnmt] <- as.character(paramdatrr$value)

# Prepare the data.frame for the final simulation schedule
sim_schedule <- data.frame(matrix(rep(temp,times=nrow(true_params)),byrow=T,ncol=length(temp)),stringsAsFactors = F)
# DO NOT CHANGE THESE TWO LINES
# If N_host is among the parameters to be inferred later update the haploid host population size
add_params <- if(i_host){cbind(true_params[,-ncol(true_params)],N_host_haploid=true_params[,"N_host"]*2)}else{true_params[,-ncol(true_params)]}
add_params <- if(i_para){cbind(add_params,N_parasite_haploid=true_params[,"N_parasite"]*2)}else{add_params}
sim_schedule[,podnmt] <-  add_params
names(sim_schedule) <- snames
# Add the key to the simulation schedule
sim_schedule <- cbind(sim_schedule,key=true_params$key)
# Calculate the population mutation rates and the number of generations to simulate as mentioned in the paper
sim_schedule$popmut_host <- as.numeric(as.character(sim_schedule$N_host))/1000
sim_schedule$popmut_para <- as.numeric(as.character(sim_schedule$N_parasite))/1000
sim_schedule$gen <- apply(cbind(as.numeric(as.character(sim_schedule$N_host)),as.numeric(as.character(sim_schedule$N_parasite))),1,max)*6
# Add the seed of the pod as a separate column
sim_schedule$seedpod <- seedpod

# Check that nsam_host and nsam_para are the same
if(sum(as.numeric(as.character(sim_schedule$nsam_host))-as.numeric(as.character(sim_schedule$nsam_para)))>0){stop1("At the current state of the pipeline nsam_host and nsam_para have to be the same")}

# If the PODgeneration is run in a different directory copy the necessary programs and scripts to the particular location
if(wdout!=getwd()){
  file.copy(optasetspods[["pod_programs"]],wdout)
}


simfname <- paste0(wdout,"/",prefix,"_input_pods.txt")
# If older versions of the pipeline exist in the particular folder add the prefix OLD everywhere to prevent that the
# files get overwritten
grestring <- "(^prefix_((true_params|pseudo_observed|input_pods)(_key|)).txt$|^(OLD_)+prefix_((true_params|pseudo_observed)(_key|)).txt$)"
gretoin <-gsub("prefix",prefix,grestring)
a <- list.files(wdout)[grep(gretoin, list.files(wdout))]
file.rename(from=paste0(wdout,"/",a),to=paste0(wdout,"/OLD_",a))
# Output the simulation schedule
write.table(sim_schedule,simfname,row.names=F,col.names=T,quote = F)


# Move to the folder where pod generation is supposed to take place and pod simulations
setwd(wdout)
# Run POD generation for the particular simulation schedule
system(paste("./launch_podgenGFG_20.sh",prefix))

# Extract summary statistics from all created pods
podstats <- lapply(1:nrow(sim_schedule),function(x){
  to_read <- paste0(sim_schedule$key[x],"/average_sumstats.out")
  if(file.exists(to_read)){
  podx <- read.table(to_read,header=T,sep="\t")
  podxkey <- cbind(podx,key=as.character(sim_schedule$key[x]))}else{podxkey <- cbind(fail=NA,key=as.character(sim_schedule$key[x]))}
  return(podxkey)
})

# Check where pod generation failed
failed <- which(sapply(podstats,ncol)==2)

# Write true param files
if(length(failed) > 0){
  write.table(true_params[-which(true_params$key%in%as.character(podstats[[failed]][,"key"])),,drop=F],paste0(wdout,"/",prefix,"_true_params_key.txt"),row.names=F,col.names=T,quote=F)
  write.table(true_params[-which(true_params$key%in%as.character(podstats[[failed]][,"key"])),-ncol(true_params),drop=F],paste0(wdout,"/",prefix,"_true_params.txt"),row.names=F,col.names=T,quote=F)
  podstats_successful <- podstats[-failed]
}else{
  write.table(true_params,paste0(wdout,"/",prefix,"_true_params_key.txt"),row.names=F,col.names=T,quote=F)
  write.table(true_params[,-ncol(true_params),drop=F],paste0(wdout,"/",prefix,"_true_params.txt"),row.names=F,col.names=T,quote=F)
  podstats_successful <- podstats
}

# Write file with Pseudo_observed_data
podstats_data <- do.call(rbind,podstats_successful)
if(is.null(podstats_data)){podstats_data <- data.frame(Error="Could not create any of the desired pods",podGeneration="")}
# Once including the key
write.table(podstats_data,paste0(prefix,"_pseudo_observed_key.txt"),row.names=F,quote=F,sep="\t")
# Once excluding the key
write.table(podstats_data[,-ncol(podstats_data),drop=F],paste0(prefix,"_pseudo_observed",".txt"),
            row.names=F,quote=F,sep="\t")


# Compress the msms-outputfiles and files containing the outputs for the frequency paths
for(key in sim_schedule$key){
  zip(zipfile = paste0(key,"/",paste("Raw_data",key,"zipped.zip",sep="_")), flags="-Tm",files = paste0(key,"/",list.files(key,pattern = "^seq|^GFGP|^GFGH")))
}