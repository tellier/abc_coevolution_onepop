\documentclass[12pt,a4paper,onehalfspacing]{article}
%\usepackage{ngerman}
\usepackage{hyperref}
\hypersetup{colorlinks,
	linkcolor={blue!50!black},
	citecolor={black},
	urlcolor={blue!80!black}}
\usepackage{chngcntr}	% package to set counter for tables and figures
	\counterwithin{figure}{section}	% figures are numbered within each section from 1 to n
	\counterwithin{table}{section} % figures are numbered within each section from 1 to n, put * after counterwithin and section number will not appear
\usepackage{booktabs} % for nice tables
	\setlength\heavyrulewidth{1.5pt} %changes thickness of toprule and bottomrule
	\newcommand{\ra}[1]{\renewcommand{\arraystretch}{#1}}
\usepackage[justification=raggedright,singlelinecheck=false,labelfont={bf,sf}]{caption}  % to change appearance of captions 
\usepackage{epsfig}		% to include eps graphics
%\usepackage{titlesec}
%	\titleformat*{\section}{\Large\bfseries\sffamily}
%	\titleformat*{\subsection}{\large\bfseries\sffamily}
%	\titleformat*{\subsubsection}{\bfseries\sffamily}

\usepackage{amsmath}
\usepackage{array}	% 
\usepackage{longtable} %table over several pages
\usepackage{listings}	% inclusion of code
\usepackage{color}	% allows specification of colors
	\definecolor{mygreen}{rgb}{0,0.6,0}
	\definecolor{mygray}{rgb}{0.4,0.4,0.4}
	\definecolor{mygray2}{rgb}{0.7,0.7,0.7}
	\definecolor{mymauve}{rgb}{0.58,0,0.82}
\usepackage{colortbl} % color in table
	\arrayrulecolor{mygray}
\usepackage{courier} % includes font courier
\usepackage{float} % package for stopping figures to float around
\usepackage{amsmath}
\usepackage{forest}
\usepackage[customcolors]{hf-tikz}
\setlength{\parindent}{0cm}

\renewcommand*{\familydefault}{\sfdefault}

\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\ttfamily\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{black},       % keyword style
  language=R,                 	% the language of the code
  otherkeywords={*,...},           % if you want to add more keywords to the set
  numbers=none,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,	                   % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}
\usepackage{natbib}

\newenvironment{myfont}{\fontfamily{pcr}\selectfont}{\par}

\author{
  Hanna M\"arkle
}
\title{Manual pseudo-observed data (POD) set generation for the ABC Coevolution pipeline}


\begin{document}

\maketitle
\section{Introduction}
The pseudo-observed data set (POD) generator can be used to generate PODs for the ABC Coevolution pipeline. 
During POD-generation pseudo-observed polymorphism data from putative coevolutionary loci of the host and the parasite will be simulated and summarized using summary statistics. The simulations are based on coupling a coevolutionary model with coalescent simulations. At the the moment a gene-for-gene model (for details see \cite{Tellier2007a}) is used as coevolutionary model. This coevolutionary model is implemented in a self-written C++-program. Coalescent simulations are performed using a modified version of msms \citep{Tellier2014}. \\
Pseudo-observed data can be either simulated for a single parameter combination or for a set of parameter combinations. In case of the latter it is possible to either simulate several pseudo-observed data sets at once by sequentially varying a single parameter while fixing all other parameters. Or several parameters can be varied at the same time and pseudo-observed data are simulated for all possible combinations of the varied parameters. \\
It is possible to simulate pseudo-observed data sets from repeated experiments. In this case the statistics are first calculated separately for each replicate and are subsequently averaged over all replicates of a single parameter combination. \\
PODs can be generated for varying values of all parameters which are highlighted with yes in column \textbf{inferable} of table \ref{variabletable}. In addition, it is possible to change the value of any parameter in \ref{variabletable} within the range of possible values.

\begin{table}[ht]
\centering
\caption{List of available parameters in the pipeline.}
\begin{tabular}{lp{0.5cm}lllp{0.5cm}p{6cm}}
  \hline
 name & data\-type & value & min & max & infer\-able & Description\\ 
  \hline
nsam\_host & 1 & 50 & 5 & 200 & no & sample size host \\ 
nsam\_para & 1 & 50 & 5 & 200 & no & sample size parasite \\ 
nspg & 1 & 250 & 1 & 250 & no & number of simulations per grid point \\ 
nsimpgu & 1 & 200 & 1 & 200 & no & number of simulations per grid point used to calculate the summary statistics \\ 
N\_host & 1 & 5000 & 250 & 100,000 & yes & diploid host population size \\ 
N\_parasite & 1 & 5000 & 250 & 100,000 & yes & diploid parasite population size\\ 
popmut\_host & 1 & 5 & 1e-2 & 400 & yes & population mutation rate host. Note that in the current version this parameter is always set to N\_host/1000 \\ 
popmut\_para & 1 & 5 & 1e-2 & 400 & yes & population mutation rate parasite. Note that in the current version this parameter is always set to N\_parasite/1000 \\ 
cH & 0 & 0.05 & 0 & 1 & yes & cost of resistance \\ 
s & 0 & 0.2 & 0 & 1 & yes & cost of infection\\ 
cP & 0 & 0.10 & 0 & 1 & yes & cost of virulence\\ 
cost &  0 & 1.00 & 1 & 1 & yes & cost of non-successful attack\\ 
R0 & 0 & 0.20 & 0 & 1 & yes & initial frequency of RES-hosts\\ 
a0 & 0 & 0.20 & 0 & 1 & yes & initial frequency of inf-parasites\\ 
gen & 1 & 30000 & 10 & 600,000 & no & number of generations to simulate. Note that in the current version this parameter is always set to 3*(N\_host + N\_parasite + abs(N\_host - N\_parasite)) \\ 
psi & 0 & 1.00 & 0 & 1 & yes & auto-infection rate if model=1. Parameter is ignored if model=0.\\ 
Rtor & 0 & 1e-5 & 0 & 1e-1 & yes & mutation rate RES to res\\ 
rtoR & 0 & 1e-5 & 0 & 1e-1 & yes & mutation rate res to RES\\ 
Atoa & 0 & 1e-5 & 0 & 1e-1 & yes & mutation rate INF to inf\\ 
atoA & 0 & 1e-5 & 0 & 1e-1 & yes & mutation rate inf to INF\\ 
prec & 0 & 1e-5& 1e-9 & 1e-1 &  yes & precision used to simulate coevolutionary trajectory\\ 
model & 1 & 0 & 0 & 1 & yes & model to run. 0=monocyclic model (Tellier and Brown 2007); 1=polycyclic model (Tellier and Brown 2007). Depending on the used setting for psi it is either the polycyclic autoinfection model or the polycyclic auto-allo-infection model.\\ 
rDrift & 1 & 1 & 0 & 1 & yes & include drift in generation of allele frequency path \\ 
   \hline  
  \
  \label{variabletable}
\end{tabular}
\end{table}

\section{Getting started}
\subsection{System requirements}
POD generation is intended to be run on Linux.
In order to create PODs successfully it is necessary that R (POD-generation was tested using R (R Core Team 2019) version 3.4.3 and 3.6.2) is installed on the system together with the R-package plyr (Wickham 2011). Shell scripts have been tested under bash versions 4.4.12 and 4.4.20.


\subsection{Necessary scripts/programs/files}
This folder \path{ABC_GFG_Pod_generation} contains the following scripts/programs:\\
\begin{small}
\begin{forest}
  for tree={
    font=\ttfamily,
    grow'=0,
    child anchor=west,
    parent anchor=south,
    anchor=west,
    calign=first,
    edge path={
      \noexpand\path [draw, \forestoption{edge}]
      (!u.south west) +(7.5pt,0) |- node[fill,inner sep=1.25pt] {} (.child anchor)\forestoption{edge label};
    },
    before typesetting nodes={
      if n=1
        {insert before={[,phantom]}}
        {}
    },
    fit=band,
    before computing xy={l=15pt},
  }
[\textbf{ABC\_Pod\_Generation}
  [Another\_Cpp]    
  [extractSumstatsABCRfuns\_pods.R]
  [Input\_pod\_generator.R]
  [Input\_pod\_generator.R]
  [Input\_pod\_generator\_theta.R]
  [Input\_pod\_generator\_theta\_20.R]
  [Input\_pod\_generator\_theta\_20\_Nhost\_fixed.R]
  [Input\_pod\_generator\_20.R]
  [launch\_podgenGFG\_20.sh]
  [launch\_podgenGFG.sh]
  [msms3.2rc-b44-pav.jar]  
  [template\_input\_simulationProg.txt]
]  
\end{forest}
\end{small}

The files are described in table \ref{fpspodgen}.

\begin{table}[h!]
\caption{Description of files/programs and scripts which are used to generate the pods.}
\begin{tabular}{p{6cm}p{8cm}}
\hline
Name & description \\
\hline
Another\_Cpp & C++-program to simulate allele frequency trajectories under a gene-for-gene model\\
extractSumstatsABCRfuns\_pods.R & R-script to extract summary statistics from simulated sequence data\\
Input\_pod\_generator\_20.R & R-script to setup POD generation pipeline and to launch POD-generation. Note this script has been setup in a way that it allows to simulate all pseudo-observed data sets which have been generated for the paper.\\
launch\_podgenGFG\_20.sh & Shell-script to simulate the pods\\
msms3.2rc-b44-pav.jar & modified version of msms to simulate sequence data from allele frequency trajectories \\
template\_input\_simulationProg.txt & template for generating the POD-simulation schedule. Do not remove.\\
\hline
\end{tabular}
\label{fpspodgen}
\end{table}

\section{Running pod generation}
	
\subsection{Running basic pod generation}
	The basic syntax to launch POD-generation is:
	\begin{lstlisting}[language=bash]
	Rscript --vanilla Input_pod_generator_20.R -i nameofinferparam min max nbsteps
	\end{lstlisting}
	This will create POD(s) with the intention that you want to infer the parameter {\myfont nameofinferparam} when running the ABC. The standard settings as specified in Tab. \ref{variabletable} are used for all other parameters. {\myfont nbsteps} indicates for how many different values of the parameter {\myfont nameofinferparam} PODs will be created. These values range from {\myfont min} to {\myfont max} in equally spaced steps. For example specifying:
	\begin{lstlisting}[language=bash]
	Rscript --vanilla Input_pod_generator_20.R -i s 0.1 0.9 9
	\end{lstlisting}
	will simulate PODs for a cost of infection \textit{s} being equal to 
	\begin{itemize}
    \setlength\itemsep{-0.5em}
	\item s=0.1
	\item s=0.2
 	\item s=0.3
 	\item s=0.4
 	\item s=0.5
 	\item s=0.6
 	\item s=0.7
 	\item s=0.8
 	\item s=0.9	
	\end{itemize}
	 
	For all other parameters the standard values (see Tab.~\ref{variabletable}) will be used.
	By running this basic command the following files and directories will be created in folder \path{ ABC_GFG_Pod_Generation}:
	
	\begin{figure}
	\begin{scriptsize}
	\begin{forest}
	  for tree={
	    font=\ttfamily,
	    grow'=0,
	    child anchor=west,
	    parent anchor=south,
	    anchor=west,
	    calign=first,
	    edge path={
	      \noexpand\path [draw, \forestoption{edge}]
	      (!u.south west) +(7.5pt,0) |- node[fill,inner sep=1.25pt] {} (.child anchor)\forestoption{edge label};
	    },
	    before typesetting nodes={
	      if n=1
	        {insert before={[,phantom]}}
	        {}
	    },
	    fit=band,
	    before computing xy={l=15pt},
	  }
	[\textbf{ABC\_Pod\_Generation}
	  [\path{ABC_GFG_seedpod_1_input_pods.txt}]
	  [\path{ABC_GFG_seedpod_1_pseudo_observed_key.txt}]
	  [\path{ABC_GFG_seedpod_1_pseudo_observed.txt}]
	  [\path{ABC_GFG_seedpod_1_true_params_key.txt}]
	  [\path{ABC_GFG_seedpod_1_true_params.txt}]
	  [\textbf{Pod\_ABC\_GFG\_seedpod\_1\_s\_0.1}
	  	[\path{param_used.txt}]
	  	[\path{mrca_times.out}]
	  	[\path{sumstats_pr.out}]
	  	[\path{(average_sumstats.out)}]
	  	[\path{(sucruns_used.out)}]
	  	[\path{logs_together.out}]
	  	[\path{Raw_data_Pod_ABC_GFG_seedpod_1_s_0.1_zipped.zip}]
	  	]
	  [\textbf{Pod\_ABC\_GFG\_seedpod\_1\_s\_0.2}
	  	[\path{param_used.txt}]
	  	[\path{mrca_times.out}]
	  	[\path{sumstats_pr.out}]
	  	[\path{(average_sumstats.out)}]
	  	[\path{(sucruns_used.out)}]
	  	[\path{logs_together.out}]
	  	[\path{Raw_data_Pod_ABC_GFG_seedpod_1_s_0.2_zipped.zip}]
	  ]
	  [...]
	  [\textbf{Pod\_ABC\_GFG\_seedpod\_1\_s\_0.9}
	  	  	[\path{param_used.txt}]
	  	  	[\path{mrca_times.out}]
	  	  	[\path{sumstats_pr.out}]
	  	  	[\path{(average_sumstats.out)}]
	  	  	[\path{(sucruns_used.out)}]
	  	  	[\path{logs_together.out}]
	  	  	[\path{Raw_data_Pod_ABC_GFG_seedpod_1_s_0.1_zipped.zip}]
	  ]
	]  
	\end{forest}
	\end{scriptsize}
	\caption{Files and directories created during standard POD-generation. By default these directories/files are produced in the directory from which POD generation is launched.}
	\label{FileDirStandard}
	\end{figure}

\begin{itemize}
\item \path{ABC_GFG_seedpod_1_input_pods.txt} contains the parameter combination(s) which have been used to simulate the POD(s). The column \textbf{key} gives the name of the directory to which the POD for the given parameter combination has been placed to.

\item \path{ABC_GFG_seedpod_1_pseudo_observed_key.txt} contains the average of the statistics for all successfully created POD(s). The last column reference to the corresponding POD simulation-folder from which these average statistics have been extracted. If the POD for a given parameter combination could not be generated successfully the summary statistics of this POD will not be included into this file.

\item \path{ABC_GFG_seedpod_1_pseudo_observed.txt} is the same (same line and column order) as \path{ ABC_GFG_pseudo_observed_key.txt} but without the column key. This file can be used as observed data file when launching the ABC coevolution pipeline.

\item \path{ABC_GFG_seedpod_1_true_params_key.txt} contains the true parameter values for each POD which are to be inferred in the ABC coevolution pipeline. The corresponding POD-ID is given in column key. If a POD for a given parameter combination could not be generated successfully this POD and its true parameters will not be included to this file.

\item \path{ABC_GFG_seedpod_1_true\_params.txt} is the same (same line and column order) as \path{ ABC_GFG_seedpod_1_true_params_key.txt} but without the column key. This file can be used as true parameter file in the ABC Coevolution pipeline.
\end{itemize}
	
The folder \path{Pod_ABC_seedpod_1_GFG_s_XY} contains the results of the POD generation for cost of infection being s=XY. 

Within the folder:
\begin{itemize}
\item The zip folder \path{Raw_data_Pod_ABC_GFG_seedpod_1_s_XY_zipped.zip} contains the zipped frequency paths and sequences created for each of the {\myfont nspg} repetitions.
\item \path{param_used.txt} contains the parameters which have been used to simulate each of the {\myfont nspg} repetitions. The i-th line corresponds to the parameter combination used to simulate the data for the i-th repetition (\path{GFGH\_i.out}, \path{GFGP_i.out}, \path{seq_H_i.out},\path{seq_P_i.out} in zip folder). This file can be used to check if the seed for each of the repetitions (column 15) has been correctly specified. 
\item \path{logs_together.out} contains the parameter values which have been read by the C++-program which is used to simulate the frequency paths under the coevolutionary model. This file is based on output from the C++-program itself. This file can be compared to the file \path{param_used.txt} and provides a check for the integrity of the POD-generation.
\item \path{sumstats_pr.out} contains the calculated summary statistics of all successful repetitions (as simulations sometimes fail this number is $<=$ {\myfont nspg}). The IDs of the corresponding repetitions are given in column \textbf{REP\_ID}.
\item \path{mrca_times.out} gives the time to the most recent common ancestor for all successful simulations. Again the column \textbf{REP\_ID} gives the ID of the corresponding repetition. 
\end{itemize}

If there have been at least \textit{nsimpgu} successful repetitions two additional files namely \path{average\-_sumstats.out} and \path{sucruns_used.out} are created. 

\begin{itemize}
\item \path{average_sumstats.out} contains the average of the summary statistics among replicated. These are calculated by randomly chosing {\myfont nsimpgu} simulations out of all successful repetitions. 
\item \path{sucruns_used.out} contains the IDs of the repetitions which have been used to calculate the average summary statistics in file \path{average_sumstats.out}.
\end{itemize}

\subsection{Specifying several varying parameters using option -i several times}
It is possible to vary several parameters simultaneously during POD generation.
The basic command to achieve this is:
	\begin{lstlisting}[language=bash]
	Rscript --vanilla Input_pod_generator_20.R -i nameofinferparam1 min1 max1 nbsteps1 -i nameofinferparam2 min2 max2 nbsteps2 ...
	\end{lstlisting}
In this case pods will be generated for all pairwise combinations of the specified varying parameters.
If you for example type:
	\begin{lstlisting}[language=bash]
	Rscript --vanilla Input_pod_generator_20.R -i s 0.1 0.5 5 -i cH 0.2 0.4 3
	\end{lstlisting}
PODs will be generated for all combinations of s and cH as shown in table \ref{combinations}.

\begin{table}
\caption{Generated parameter combinations of $s$ and $cH$ when POD generation is launched using -i s 0.1 0.5 5 -i cH 0.2 0.4 }
\begin{tabular}{ll}
\hline
s & cH \\
\hline
0.1 & 0.2 \\
0.2& 0.2\\
0.3& 0.2\\
0.4& 0.2\\
0.5& 0.2\\
0.1&0.3\\
0.2 &0.3\\
0.3 &0.3\\
0.4 &0.3\\
0.5& 0.3\\
0.1 &0.4\\
0.2 &0.4\\
0.3 &0.4\\
0.4 &0.4\\
0.5 &0.4\\
\hline
\end{tabular}
\label{combinations}
\end{table}

For each of these combinations the other parameters will be fixed to their standard values.
If several parameters are varied simultaneously the key for the respective POD will be equal to \\ {\myfont Pod\_PREFIX\_seedpod\_1\_nameparam1\_valueparam1\_nameparam2\_valueparam2}.

\subsection{Changing pod prefix using option -prefix}
It is possible to change the prefix which is appended to all files generated during POD-generation. The default prefix is {\myfont ABC\_GFG}.
You can change this standard prefix to a user defined prefix when launching POD-generation with the option {\myfont -prefix}. This option has to be followed by a single argument namely the new prefix string {\myfont NEWPREFIX}. Using this option will replace {\myfont ABC\_GFG} by {\myfont NEWPREFIX} in all folder names and file names.

If you for example launch:
	\begin{lstlisting}[language=bash]
	Rscript --vanilla Input_pod_generator_20.R -i s 0.1 0.9 9 -prefix PREFIX
	\end{lstlisting}
the files and directories shown in figure \ref{FileDirPrefix} will be created. The string {\myfont NEWPREFIX} has to start with a letter and only contain numbers, letters and underscores are allowed. Otherwise POD-generation is aborted.

	\begin{figure}
	\begin{scriptsize}
		\begin{forest}
			for tree={
				font=\ttfamily,
				grow'=0,
				child anchor=west,
				parent anchor=south,
				anchor=west,
				calign=first,
				edge path={
					\noexpand\path [draw, \forestoption{edge}]
					(!u.south west) +(7.5pt,0) |- node[fill,inner sep=1.25pt] {} (.child anchor)\forestoption{edge label};
				},
				before typesetting nodes={
					if n=1
					{insert before={[,phantom]}}
					{}
				},
				fit=band,
				before computing xy={l=15pt},
			}
			[\textbf{ABC\_Pod\_Generation}
			[\path{NEWPREFIX_seedpod_1_input_pods.txt}]
			[\path{NEWPREFIX_seedpod_1_pseudo_observed_key.txt}]
			[\path{NEWPREFIX_seedpod_1_pseudo_observed.txt}]
			[\path{NEWPREFIX_seedpod_1_true_params_key.txt}]
			[\path{NEWPREFIX_seedpod_1_true_params.txt}]
			[\textbf{Pod\_NEWPREFIX\_seedpod\_1\_s\_0.1}
			[\path{param_used.txt}]
			[\path{mrca_times.out}]
			[\path{sumstats_pr.out}]
			[\path{(average_sumstats.out)}]
			[\path{(sucruns_used.out)}]
			[\path{logs_together.out}]
			[\path{Raw_data_Pod_NEWPREFIX_seedpod_1_s_0.1_zipped.zip}]
			]
			[\textbf{Pod\_NEWPREFIX\_seedpod\_1\_s\_0.2}
			[\path{param_used.txt}]
			[\path{mrca_times.out}]
			[\path{sumstats_pr.out}]
			[\path{(average_sumstats.out)}]
			[\path{(sucruns_used.out)}]
			[\path{logs_together.out}]
			[\path{Raw_data_Pod_NEWPREFIX_seedpod_1_s_0.2_zipped.zip}]
			]
			[...]
			[\textbf{Pod\_NEWPREFIX\_seedpod\_1\_s\_0.9}
			[\path{param_used.txt}]
			[\path{mrca_times.out}]
			[\path{sumstats_pr.out}]
			[\path{(average_sumstats.out)}]
			[\path{(sucruns_used.out)}]
			[\path{logs_together.out}]
			[\path{Raw_data_Pod_NEWPREFIX_seedpod_1_s_0.1_zipped.zip}]
			]
			]  
		\end{forest}
	\end{scriptsize}
	\caption{Files and directories if option {\myfont -prefix} with argument {\myfont NEWPREFIX} is used. Compare this Fig. to Fig.~\ref{FileDirStandard}}
	\label{FileDirPrefix}
	\end{figure}

\subsection{Changing values of fixed parameters using option -sc}
It is possible to change the values of fixed parameters while generating PODs. In order to change the value of a parameter from the standard value to a user defined value the option {\myfont -sc} has to be used. This option has to be followed by two parameters, namely the name of the parameter which you would like to change and the new value for the parameter. \\
Let's assume you would like to simulate POD(s) for changing cost of infections (s=0.1,0.2,0.3) and you would like to change the cost of virulence from the standard value cP=0.1 to cP=0.2. Additionally, you would like to use a new prefix ABC\_GFG\_cP\_02 to indicate this change.
In order to achieve this you have to launch the following command:
	\begin{lstlisting}[language=bash]
	Rscript --vanilla Input_pod_generator_20.R -i s 0.1 0.3 3 -sc cP 0.2 -prefix ABC_GFG_cP_02
	\end{lstlisting}
Please note: If you specify a parameter with options -i and {\myfont -sc} POD-generation is aborted.
For example:
	\begin{lstlisting}[language=bash]
	Rscript --vanilla Input_pod_generator_20.R -i s 0.1 0.3 3 -sc s 0.2 -prefix ABC_GFG_cP_02
	\end{lstlisting}
would abort POD-generation.
	
\subsection{Changing directory in which PODs should be placed to using option -wdout}

You can change the working directory to which results of POD-generation should be placed using the option {\myfont -wdout}. This option has to be followed by a single argument namely the absolut path to the directory where the output of POD-generation should be placed to.	

\subsection{Changing seed for running the POD generation using the option -seedpod}
You can use the option {\myfont -seedpod} to specify the 'master-seed' which should be used to run POD-generation for the specified parameter combinations. This 'master-seed' is used as follows. The seed for generating the frequency path for the $i$-th replicate of a particular parameter combination is calculated as $j=\text{master-seed}+1$. This seed $j$ is also used for running the host msms-simulation for the $i$-th replicate. The parasite msms-simulation for the $i$-th replicate is run with a seed being equal to the $j+1000$. 



\bibliographystyle{natbib}%%%%Bibliography style file
\bibliography{Literature}%%%bibliography file(.bib)

\end{document}
