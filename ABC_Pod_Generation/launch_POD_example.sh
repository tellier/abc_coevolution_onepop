#!/bin/bash

# Get current working directory (directory from which this script is launched)
curdir=`pwd`

# Launch the setup of POD generation and run the POD generation
Rscript --vanilla Input_pod_generator_20.R -i s 0.25 0.25 1 -sc nsimpgu 30 -sc nspg 50 -sc model 1 -sc psi 1 -prefix ExamplePOD -wdout ${curdir}/ExamplePOD
