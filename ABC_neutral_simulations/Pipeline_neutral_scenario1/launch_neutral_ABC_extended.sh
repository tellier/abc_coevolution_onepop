#!/bin/bash

# Read input file created by the ABCsampler
file=$1
read -a param < ${file}
# Read first column as host seed
seedhost=${param[0]}
echo ${seedhost}
# Read second column as diploid host population size
N_host=${param[1]}
# Read third column as diploid parasite population size
N_parasite=${param[2]}
# Read fourth column as population mutation rate for the host
theta_host=${param[3]}
# Read fifth column as population mutation rate parasite
theta_parasite=${param[4]}
# Number of repetitions per simulation
nsimpgu=30
# Sample size for the host and the parasite (diploids)
nsam=50


# Run coalescent simulation for the host for a sample size of nsam and for nsimpgu independent host loci
java -jar ./msms3.2rc-b44-pav.jar -N ${N_host} -ms ${nsam} ${nsimpgu} -t ${theta_host} -seed ${seedhost} > seq_host_${seedhost}.txt

# Prepare input for SFS and sumstat calculation
tail -n +5 seq_host_${seedhost}.txt |sed '/^ *$\|^\/\//d' | awk '{FS=""}BEGIN{cnt=0}{$0=$0;if(/p|s/){if(/e/){if(cnt>0){z="Record_"cnt"\t"a[1];delete a[1];for(q in a){z=z"\t"a[q]};print z;delete a};cnt++;next}{next}}{for(i=1;i<=NF;i++){a[i]+=$i}}}END{z="Record_"cnt"\t"a[1];delete a[1];for(q in a){z=z"\t"a[q]};print z}' > host_${seedhost}.txt
# Add 100000 to the host seed to obtain the seed for the coalescent simulations of the parasite
 	seedpara=$((${seedhost}+100000))
# Run the coalescent simulation for the parasite with a constant population size of N_paraiste	
java -jar ./msms3.2rc-b44-pav.jar -N ${N_parasite} -ms ${nsam} ${nsimpgu} -t ${theta_parasite} -seed ${seedpara} > seq_parasite_${seedpara}.txt

# Prepare input for SFS and sumstat calculation
tail -n +5 seq_parasite_${seedpara}.txt |sed '/^ *$\|^\/\//d' | awk '{FS=""}BEGIN{cnt=0}{$0=$0;if(/p|s/){if(/e/){if(cnt>0){z="Record_"cnt"\t"a[1];delete a[1];for(q in a){z=z"\t"a[q]};print z;delete a};cnt++;next}{next}}{for(i=1;i<=NF;i++){a[i]+=$i}}}END{z="Record_"cnt"\t"a[1];delete a[1];for(q in a){z=z"\t"a[q]};print z}' > parasite_${seedpara}.txt

# Calculate the summary statistics
Rscript --vanilla ExtractSFS_calculate_sumstats_neutral.R host_${seedhost}.txt parasite_${seedpara}.txt ${nsam} ${nsimpgu}

if [ `echo ${seedhost} % 2500 | bc` == 0 ]
then 
	tar -czvf sequences_host_${seedhost}.tar.gz seq_host_* --remove-files
	tar -czvf preparedSFS_host_${seedhost}.tar.gz host_* --remove-files
	tar -czvf sequences_parasite_${seedpara}.tar.gz seq_parasite_* --remove-files
	tar -czvf preparedSFS_parasite_${seedpara}.tar.gz parasite_* --remove-files
fi

# Set the seed for the next simulation in in.txt
seednew=$((${seedhost}+1))
perl -i -slane '{@F[0]=$i; print join("\t",@F)}' -- -i=${seednew} in.txt




