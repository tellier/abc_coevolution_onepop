---
title: "ABC model choice neutral simulations Scenario 2"
author: "Hanna Märkle"
date: "February 09, 2020"
output:
  pdf_document: default
  html_document: default
---


This folder contains the scripts/programs which can been used to simulate the data for neutral loci under **Scenario 2** and for **r=30**

Data in folder:

* ``ExtractSFS_calculate_sumstats_neutral.R``  
Script to extract the summary statistics for each simulated parameter combination
* ``msms3.2rc-b44-pav.jar``  
Modified (Tellier, Moreno-Gamez and Stephan 2014) version of msms (Ewing and Hermisson 2010)
* ``launch_neutral_ABC_constPop.sh``   
Commented version of the shell script which launches the neutral msms simulations for Scenario 2 and launches the calculation of the corresponding summary statistics
* ``README.rmd``  
Readme file

The pipeline works as follows:

All parameters for running the neutral simulations are specified within the shell script. The script will run 100,000 independent simulations with r=30 independent loci (parameter nsimpgu in script) each for the host and the parasite. The seed for the i-th simulation msms-simulation in the host is set to i and the seed for the i-th msms-simulation for the parasite is set to be i+100,000.
The R-script ``ExtractSFS_calculate_sumstats_neutral.R`` is used to calculate the required summary statitics for the i-th pair of host and parasite simulations. The output of this script is a file called ``average_sumstats.out`` which is append to the file ``ABC_neutral_sampling_const_popsize.sim`` 

### How to run the simulations for r=10
1. Open the shell script: ``launch_neutral_ABC_constPop.sh``.
2. Change the line which says ``nsimpgu=30`` to ``nsimpgu=10``.


### References

Ewing, G. and Hermisson, J. (2010): MSMS: a coalescent simulation program including  recombination, demographic structure and selection at a single locus. Bioinformatics 26(16): 2064-2065.


Tellier, A., Moreno-Gamez, S. and Stephan, W. (2014): Speed of adaptation and genomic footprints of host-parasite coevolution under arms race and trench warfare dynamics. Evolution 68(8): 2211-2224.


