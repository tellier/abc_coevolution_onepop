#!/bin/bash

# Set diploid host population size to 5000
N_host=5000
# Set diploid parasite population size
N_parasite=5000
# Set the population mutation rate for host
theta_host=5
# Set the population mutation rate for the parasite
theta_parasite=5
# Set the number of repetitions per simulation
nsimpgu=30
# Set the haploid host and parasite sample size
nsam=50
# Define the name of the output file
outfile="ABC_neutral_sampling_const_popsize.sim"

# Run 100,000 simulations
for seedhost in {1..100000}
do
	echo ${seedhost}
	# Run msms simulation with constant population size for a sample size of 50 haploid host, and 30 independent loci
	java -jar ./msms3.2rc-b44-pav.jar -N ${N_host} -ms ${nsam} ${nsimpgu} -t ${theta_host} -seed ${seedhost} > seq_host_${seedhost}.txt
	# Prepare msms output for extracting the SFS and sumstats
	tail -n +5 seq_host_${seedhost}.txt |sed '/^ *$\|^\/\//d' | awk '{FS=""}BEGIN{cnt=0}{$0=$0;if(/p|s/){if(/e/){if(cnt>0){z="Record_"cnt"\t"a[1];delete a[1];for(q in a){z=z"\t"a[q]};print z;delete a};cnt++;next}{next}}{for(i=1;i<=NF;i++){a[i]+=$i}}}END{z="Record_"cnt"\t"a[1];delete a[1];for(q in a){z=z"\t"a[q]};print z}' > host_${seedhost}.txt
	# Add 100,000 to the host seed to obtain the seed for the msms simulations for the parasite
		seedpara=$((${seedhost}+100000))
	# Run simulation for the parasite	
	java -jar ./msms3.2rc-b44-pav.jar -N ${N_parasite} -ms ${nsam} ${nsimpgu} -t ${theta_parasite} -seed ${seedpara} > seq_parasite_${seedpara}.txt
	tail -n +5 seq_parasite_${seedpara}.txt |sed '/^ *$\|^\/\//d' | awk '{FS=""}BEGIN{cnt=0}{$0=$0;if(/p|s/){if(/e/){if(cnt>0){z="Record_"cnt"\t"a[1];delete a[1];for(q in a){z=z"\t"a[q]};print z;delete a};cnt++;next}{next}}{for(i=1;i<=NF;i++){a[i]+=$i}}}END{z="Record_"cnt"\t"a[1];delete a[1];for(q in a){z=z"\t"a[q]};print z}' > parasite_${seedpara}.txt

	# Extract the sumstats, Rscript writes the averages of the sumstats of the r=30 repetitions to the file average_sumstats.out
	Rscript --vanilla ExtractSFS_calculate_sumstats_neutral.R host_${seedhost}.txt parasite_${seedpara}.txt ${nsam} ${nsimpgu}

	
	if [  ${seedhost} == 1 ]
	then
		# Create the output file for the summary statistics
		cat average_sumstats.out > ${outfile}
	else
		# Else append the line with the summary statistics of average_sumstats.out to the simulation file
		tail -n +2 average_sumstats.out >> ${outfile}
	fi

	if [ `echo ${seedhost} % 2500| bc` == 0 ]
	then 
		tar -czvf copo5000_sequences_host_${seedhost}.tar.gz seq_host_* --remove-files
		tar -czvf copo5000_preparedSFS_host_${seedhost}.tar.gz host_* --remove-files
		tar -czvf copo5000_sequences_parasite_${seedpara}.tar.gz seq_parasite_* --remove-files
		tar -czvf copo5000_preparedSFS_parasite_${seedpara}.tar.gz parasite_* --remove-files
	fi
done 





