# Hanna Maerkle
# 02/09/2020
# This script can be used to run a model cross-validation and model choice for data from Scenario 1
# The script requires the following input:
# 1. File with neutral simulations Scenario 1 (either r=10 or r=30)
# 2. File with ABC simulations for the coevolutionary model for Scenario 1 (either r=10 or r=30)
# 3. File with pseudo-observed data set(s) for the studied amount of repetitions for Scenario 1. You have to provide the file ...pseudo_observed_key.txt
# as provided when running the POD generator


require("abc")
require("plyr")
require("reshape2")
require("ggplot2")
#require("grid")
#require("gtable")
#require("gridExtra")


# Set working directory
setwd("~/")

# Absolute path to file with simulations for neutral loci
neutral_name <- ""

# Read simulation data from neutral model
samp_neutral <- read.table(neutral_name,header=T) 
# Remove the parameter values for which simulations have been run
samp_neutral_sumstats <- samp_neutral[,which(!names(samp_neutral)%in%c("Sim","N_host","N_parasite","popmut_host","popmut_para"))]
# Replace parasite by para
names(samp_neutral_sumstats) <- sub("parasite","para",names(samp_neutral_sumstats))
# Add new column called model
samp_neutral_sumstats$model <- "neutral"

# # Absolute path file with coevolution simulations for the particular scenario
coevolution_name <- ""
# Read simulation data from coevolution model
samp_coevolution <- read.table(coevolution_name,header=T)
# Get the summary statistics only
# Get the summary statistics only
samp_coevolution_sumstats <- samp_coevolution[,which(!names(samp_coevolution)%in%c("Sim","N_host","N_parasite","s","popmut_host","popmut_para",
                      "N_host_haploid","N_parasite_haploid","gen"))]
# Add new column called coevolution
samp_coevolution_sumstats$model <- "coevolution"


# Check if the names of the summary statistics are the same now
all(names(samp_coevolution_sumstats)%in%names(samp_neutral_sumstats))&all(names(samp_neutral_sumstats)%in%names(samp_coevolution_sumstats))

# Bind the simulated data sets of both models together
simulations <- rbind(samp_neutral_sumstats,samp_coevolution_sumstats)
# Construct the model identifier
model_simulations <- simulations$model
# Remove model from simulations
simulations <- simulations[,-which(names(simulations)=="model")]


# Check if one can distinguish both models by using ABC
cv.modsel <- cv4postpr(model_simulations, simulations, nval=500, tol=.01, method="rejection")

# Plot the result using the default method as implemented in the abc R-package
plot(cv.modsel)

# Define theme for ggplot
mytheme<-theme(axis.line = element_line(colour = "grey"),panel.grid.major=element_blank(),
  panel.grid.minor=element_blank(),panel.background=element_blank(),
axis.title.x=element_text(size=20,vjust=-4,face="bold"),axis.title.y=element_text(size=20,vjust=4,face="bold"),
plot.margin=unit(c(.4,2.5,.4,.4),"in"),axis.text.x=element_text(size=18,color="grey15"),axis.text.y=element_text(size=18,color="grey15"),
legend.title=element_text(size=12), 
legend.text=element_text(size=12))

tabtheme <- ttheme_default(
  core = list(fg_params=list(cex = .8),padding=unit.c(unit(6, "mm"), unit(6, "mm"))),
  colhead = list(fg_params=list(cex = .8),padding=unit.c(unit(6, "mm"), unit(6, "mm"))),
  rowhead = list(fg_params=list(cex = .8,fontface=2),bg_params=list(fill=c("white","grey","grey"))),padding = unit(c(6,6), "mm"))


# Plot the result using ggplot
modelcv_red <- data.frame(truemodel=cv.modsel$true,estimmodel=cv.modsel$estim[[1]])
counts_models <- count(modelcv_red,vars=c("truemodel","estimmodel"))

ba <- summary(cv.modsel)

pdf("CV_modelchoice_Scenario1.pdf",width=14,height=9)
    tplot <- ggplot(counts_models,aes(x=truemodel,y=freq,fill=estimmodel,order=estimmodel)) + theme_bw() + mytheme
    tplot <- tplot + geom_bar(stat="identity",position = position_stack(reverse = TRUE))
    tplot <- tplot + scale_fill_manual(values=c("lightskyblue4","lightskyblue3"),name="Model") + xlab("Model") + ylab("Frequency") 
    tplot <- tplot + ggtitle("Confusion matrix: leave-one-out cross validation ",subtitle=(paste0("Rejection, tolerance: ",cv.modsel$tols)))
    tplot <- tplot + theme(plot.title=element_text(size=20,face="bold"))
    tplot <- tplot + annotation_custom(grid.arrange(top=textGrob("Confusion matrix",x=0.1,hjust=0,vjust=-0.5,gp=gpar(fontface="bold")), tableGrob(ba$conf.matrix[[1]],theme=tabtheme)),xmin=3.4,xmax=Inf, ymin=0,ymax=100)
    grid.arrange(tplot)
dev.off()


# Write cross validation results to file
cv_summary_name <- paste0(getwd(),"/Crossvalidation_output_modelselection_Scenario1.txt")
sink(cv_summary_name); print(cv.modsel); sink()
write("#####\nNeutral_simulations\n",cv_summary_name,append=T)
write(paste0("Source file neutral: ",neutral_name,"\n"),cv_summary_name,append=T)
write("####\nCoevolution_simulations\n",cv_summary_name,append=T)
write(paste0("Source file coevolution: ",coevolution_name,"\n"),cv_summary_name,append=T)

# Plot the posterior probabilities
cv.modsel_probs <- data.frame(cv.modsel$model.probs[[1]],model=row.names(cv.modsel$model.probs[[1]]))
cv.modsel_probs_melt <- melt(cv.modsel_probs,id.vars=c("model"),variable.name = "Postmodel",value.name="Posterior_proba")
pplot <- ggplot(cv.modsel_probs_melt,aes(x=model,y=Posterior_proba,fill=Postmodel))+ geom_boxplot()
pplot <- pplot + theme_bw() + mytheme
pplot <- pplot + xlab("True model") + ylab("Posterior probability") + scale_fill_manual(values=c("lightskyblue4","lightskyblue3"),name="model")
pplot

# Display the summary
summary(cv.modsel)

# Get the model probabilities for the samples used for the cross-validation
# Coevolution model only
model_probs_coevolution <- cv.modsel$model.probs[[1]][which(attributes(cv.modsel$model.probs[[1]])$dimnames[[1]]=="coevolution")]

# Check for which samples the cross-validation has been done
line_sel <- cv.modsel$cvsamples
# Check the s values of the coevolution simulations
line_sel_coevol <- line_sel[grep(glob2rx("coevolution*"),names(line_sel))] - nrow(samp_neutral_sumstats)
samp_coevolution_CV <- samp_coevolution[line_sel_coevol,c("s","N_host","N_parasite")]

# Define theme for ggplot
mytheme2 <-theme(axis.line = element_line(colour = "grey"),panel.grid.major=element_blank(),
               panel.grid.minor=element_blank(),panel.background=element_blank(),
               axis.title.x=element_text(size=20,vjust=-4,face="bold"),axis.title.y=element_text(size=20,vjust=6,face="bold"),
               plot.margin=unit(c(.4,.4,.4,.4),"in"),axis.text.x=element_text(size=18,color="grey15"),axis.text.y=element_text(size=18,color="grey15"),
               legend.text=element_blank(),plot.title=element_text(size=20,face="bold"))


pdf("Crossvalidation_ability_distinguish_models_500simul_Scenario1.pdf",width=10,height=8)
  par(mfrow=c(1,1),mar=c(8,5,6,2))
  plot(cv.modsel)
  
  # Plot the simulations
  par(mar=c(5,6,2,2),mfrow=c(2,2))
  model_probs_coevolution_params <- cbind(model_probs_coevolution,samp_coevolution_CV) 
  plot(model_probs_coevolution_params$s,model_probs_coevolution_params$model_probs_coevolution,xlab="Cost of infection (s)",ylab="Coevolution model\nposterior probability")
  abline(0.5,0,col="red")
  plot(model_probs_coevolution_params$N_host,model_probs_coevolution_params$model_probs_coevolution,xlab="Diploid host population size",ylab="Coevolution model\nposterior probability")
  abline(0.5,0,col="red")
  plot(model_probs_coevolution_params$N_parasite,model_probs_coevolution_params$model_probs_coevolution,xlab="Diploid parasite population size",ylab="Coevolution model\nposterior probability")
  abline(0.5,0,col="red")
  plot.new()
  misclas_coevol <- model_probs_coevolution_params[which(model_probs_coevolution_params$model_probs_coevolution<=0.5),c("s","N_host","N_parasite")]
  legend("topleft",title="Params misclassified simuls",text.width = 0.08,legend=c('s', round(misclas_coevol[,"s"],digits=2), 'N_host',misclas_coevol[,"N_host"], 'N_para', misclas_coevol[,"N_parasite"]),ncol=3,bty="n")
dev.off()

# Read PODs from file
PODS <- read.table("~/PODS_ABC_Polycyclic_auto_only_pseudo_observed_key.txt",header=T)
# Construct row names
row.names(PODS) <- PODS$key
# Replace the PREFIX for the PODs in the row names 
row.names(PODS) <- sub("Pod_PODS_ABC_Polycyclic_auto_only_","",row.names(PODS))
PODS <- PODS[,-which(names(PODS)=="key")]

mcpods_list <- list()
postprocess_list <- list()


pdf("Model_choice_coevolution_Scenario1.pdf",width=12,height=9.5)
for(tole in seq(0.01,0.01,by=0.01)){
  # Run the mode choice for each POD
  mcpods <- lapply(1:nrow(PODS),function(x){
    postpr(PODS[x,], model_simulations, simulations, tol=tole, method="rejection")
  })
  
  # Write the whole output list into a file
  sink(paste0("Model_choice_Scenario1_tol_",tole,".txt"))
  print(mcpods)
  sink()
  
  # Condense the information
  mcpods_postproba <- lapply(mcpods,function(x){
    a <- summary(x,print=F)
    posterior  <- a[["BayesF"]]["coevolution","neutral"]
    proba <- a[["Prob"]]["coevolution"]
    if(is.infinite(posterior)){posterior <- NA} else {posterior <- as.numeric(posterior)}
    return(c(posterior=posterior,proba=proba))
  })
  
  # Put the results for all PODs into a single data frame
 mcpods_postproba_dat <- do.call(rbind.data.frame,mcpods_postproba)
  names(mcpods_postproba_dat) <- c("BayesF","proba_coevolution")
  
  # Get the true cost of infection which has been used to simulate each POD
  s_costs <- sapply(row.names(PODS),function(x){
    a <- strsplit(x,"_")[[1]]
    s_cost <- as.numeric(a[which(a=="s")+1])
    
  })
  
  # Combine into a single data frame
  postprocess <- data.frame(mcpods_postproba_dat,s=s_costs,full_key=row.names(PODS))
  bplot <-  ggplot(postprocess,aes(x=as.factor(s),y=proba_coevolution)) + geom_boxplot(outlier.shape = NA,fill="lightgrey") + geom_jitter(width = 0.2)
  bplot <- bplot + theme_bw() + mytheme2 + xlab("Cost of infection") + ylab("Posterior probability coevolution model") + ggtitle(paste0(tole*100,"% best simulations retained"),subtitle="")
  print(bplot)
  
  # Write the table with the condensed model choice results into a file
  write.table(postprocess,paste0("Model_choice_Scenario1_tol_",tole,".txt"),quote=F,row.names=F,col.names = T)
  
  
}
dev.off()



